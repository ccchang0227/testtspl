//
//  BtSerialOperation.h
//  TestTSPL
//
//  Created by realtouch's MBP 2019 on 2020/6/8.
//  Copyright © 2020 RealTouchApp Corp. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, BtSerialOperationType) {
    BtSerialOperationTypeUnknown,
    BtSerialOperationTypeWrite,
    BtSerialOperationTypeNotify,
    BtSerialOperationTypeRead
};

@interface BtSerialOperation : NSOperation

- (instancetype)initWithType:(BtSerialOperationType)type
                  peripheral:(nullable CBPeripheral *)peripheral
              characteristic:(nullable CBCharacteristic *)characteristic;
- (instancetype)initWithType:(BtSerialOperationType)type
                  peripheral:(nullable CBPeripheral *)peripheral
              characteristic:(nullable CBCharacteristic *)characteristic
                       value:(nullable id)value
                   writeType:(CBCharacteristicWriteType)writeType NS_DESIGNATED_INITIALIZER;

@property (readonly, nonatomic) BtSerialOperationType type;
@property (readonly, strong, nonatomic) id _Nullable value;

@property (readonly, strong, nonatomic) id _Nullable response;
@property (readonly, nonatomic) BOOL isExecuteSuccess;
@property (readonly, nonatomic) BOOL isTimeout;

- (void)complete;
- (void)completeWithResponse:(nullable id)response;

@end

NS_ASSUME_NONNULL_END
