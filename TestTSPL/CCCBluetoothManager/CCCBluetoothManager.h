//
//  CCCBluetoothManager.h
//  CCCBluetoothManager
//
//  Created by realtouch on 2017/4/25.
//  Copyright © 2017年 realtouch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCCBluetoothPeripheral.h"

FOUNDATION_EXPORT NSString *const CCCBluetoothManagerPeripheralDidDisconnectNotification;
FOUNDATION_EXPORT NSString *const CCCBluetoothManagerDisconnectedPeripheralKey;
FOUNDATION_EXPORT NSString *const CCCBluetoothManagerDisconnectReasonKey;

#pragma mark - CCCBluetoothManager

@interface CCCBluetoothManager : NSObject


#pragma mark - Property
@property (readonly, strong, nonatomic) CBCentralManager *centerManager;
@property (readonly, strong, nonatomic) NSMutableArray<__kindof CCCBluetoothPeripheral *> *discoveredPeripherals;
@property (readonly, strong, nonatomic) NSMutableArray<__kindof CCCBluetoothPeripheral *> *connectedPeripherals;

@property (readonly, nonatomic, getter=isBluetoothOn) BOOL bluetoothOn;

#pragma mark - Method
+ (CCCBluetoothManager *)sharedInstance;

- (instancetype)init UNAVAILABLE_ATTRIBUTE;
+ (instancetype)new UNAVAILABLE_ATTRIBUTE;

/** 註冊class */
- (void)registerPeripheralClass:(Class)aClass;

/** 開始掃描 */
- (void)startScan;
/** 停止掃描 */
- (void)stopScan;

/** 連接裝置 */
- (void)connectPeripheral:(__kindof CCCBluetoothPeripheral *)peripheral
                  timeout:(NSTimeInterval)timeoutInterval
               completion:(CCCBluetoothCompletionBlock)completionHandler;
/** 取消連接裝置 */
- (void)disconnectPeripheral:(__kindof CCCBluetoothPeripheral *)peripheral
                  completion:(CCCBluetoothCompletionBlock)completionHandler;

/** 超時取消連線 */
- (void)connectionTimeoutAndDisconnectPeripheral:(__kindof CCCBluetoothPeripheral *)peripheral
                                    errorMessage:(NSString *)errorMessage;

@end
