//
//  CCCBluetoothManager.m
//  CCCBluetoothManager
//
//  Created by realtouch on 2017/4/25.
//  Copyright © 2017年 realtouch. All rights reserved.
//

#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_8_0
#error CCLDatePicker doesn't support Deployement Target version < 8.0
#endif

#if !__has_feature(objc_arc)
#error CCCBluetoothManager is ARC only. Either turn on ARC for the project or use -fobjc-arc flag
#endif


#import "CCCBluetoothManager.h"


NSString *const CCCBluetoothManagerPeripheralDidDisconnectNotification = @"__CCCBluetoothManagerPeripheralDidDisconnectNotification";
NSString *const CCCBluetoothManagerDisconnectedPeripheralKey = @"__CCCBluetoothManagerDisconnectedPeripheralKey";
NSString *const CCCBluetoothManagerDisconnectReasonKey = @"__CCCBluetoothManagerDisconnectReasonKey";

@interface CCCBluetoothManager () <CBCentralManagerDelegate> {
    dispatch_source_t timeoutTimer;
}

@property (assign, nonatomic) BOOL isKeepScan;
@property (strong, nonatomic) CBCentralManager *centerManager;
@property (strong, nonatomic) NSMutableArray *discoveredPeripherals;
@property (strong, nonatomic) NSMutableArray *connectedPeripherals;

@property (strong, nonatomic) NSMutableDictionary<NSUUID *, NSError *> *customErrors;

@property (strong, nonatomic) NSString *peripheralClass;

@end

@implementation CCCBluetoothManager

+ (CCCBluetoothManager *)sharedInstance {
    static dispatch_once_t pred;
    static CCCBluetoothManager *shared = nil;
    
    dispatch_once(&pred, ^{
        shared = [[super allocWithZone:NULL] initBluetoothManager];
    });
    return shared;
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone {
    return [CCCBluetoothManager sharedInstance];
}

- (instancetype)init {
    return [CCCBluetoothManager sharedInstance];
}

- (instancetype)initBluetoothManager {
    self = [super init];
    if (self) {
        _bluetoothOn = NO;
        
        NSDictionary *option = @{ CBCentralManagerOptionShowPowerAlertKey : @YES };
        self.centerManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:option];
        self.discoveredPeripherals = [[NSMutableArray alloc] init];
        self.connectedPeripherals = [[NSMutableArray alloc] init];
        
        self.customErrors = [[NSMutableDictionary alloc] init];
        
        self.peripheralClass = NSStringFromClass([CCCBluetoothPeripheral class]);
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (void)registerPeripheralClass:(Class)aClass {
    if (![aClass isSubclassOfClass:[CCCBluetoothPeripheral class]]) {
        self.peripheralClass = NSStringFromClass([CCCBluetoothPeripheral class]);
        return;
    }
    
    self.peripheralClass = NSStringFromClass(aClass);
}

- (void)startScan {
    self.isKeepScan = YES;
    [self _startScan];
}

- (void)_startScan {
    if(self.centerManager.state == CBCentralManagerStatePoweredOn) {
        NSDictionary *option = @{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES };
        [self.centerManager scanForPeripheralsWithServices:nil options:option];
        
        //搜尋指定服務的裝置
        //NSArray *uuidArray= @[ [CBUUID UUIDWithString:CCLBLEDeviceServiceUUID] ];
        //[self.centerManager scanForPeripheralsWithServices:uuidArray options:option];
        
        [self startTimeoutTimer];
    }
}

- (void)stopScan {
    self.isKeepScan = NO;
    [self _stopScan];
}

- (void)_stopScan {
    [self stopTimeoutTimer];
    [self.centerManager stopScan];
    
    [self willChangeValueForKey:@"discoveredPeripherals"];
    [self.discoveredPeripherals removeAllObjects];
    [self didChangeValueForKey:@"discoveredPeripherals"];
}

- (void)startTimeoutTimer {
    if(timeoutTimer){
        dispatch_source_cancel(timeoutTimer);
    }
    
    uint64_t interval = 3 * NSEC_PER_SEC;   //3秒重複
    //dispatch_queue_t queue = dispatch_queue_create("Bluetooth Timeout Queue", 0);
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    timeoutTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    //設定3秒後執行第一次後每3秒重複
    dispatch_source_set_timer(timeoutTimer, dispatch_time(DISPATCH_TIME_NOW, interval), interval, 0);
    dispatch_source_set_event_handler(timeoutTimer, ^() {
        [self scanTimeoutFunction];
    });
    dispatch_source_set_cancel_handler(timeoutTimer, ^{
        self->timeoutTimer = nil;
    });
    dispatch_resume(timeoutTimer);
}

- (void)stopTimeoutTimer {
    if(timeoutTimer){
        dispatch_source_cancel(timeoutTimer);
    }
}

- (void)scanTimeoutFunction {
    [self.discoveredPeripherals enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
        CCCBluetoothPeripheral *bluetoothPeripheral = obj;
        NSTimeInterval currentTimestamp = [[NSDate date] timeIntervalSince1970];
        if(currentTimestamp - bluetoothPeripheral.timestamp > 3.0){
            *stop = YES;
            if(*stop) {
                [self willChangeValueForKey:@"discoveredPeripherals"];
                [self.discoveredPeripherals removeObject:bluetoothPeripheral];
                [self didChangeValueForKey:@"discoveredPeripherals"];
            }
        }
    }];
}

- (CCCBluetoothPeripheral *)connectedCCCBluetoothPeripheralWithPeripheral:(CBPeripheral *)peripheral {
    CCCBluetoothPeripheral *bluetoothPeripheral = nil;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.identifier == %@", peripheral.identifier];
    NSArray *filteredArray = [self.connectedPeripherals filteredArrayUsingPredicate:predicate];
    if (filteredArray.count > 0) {
        bluetoothPeripheral = [filteredArray firstObject];
    }
    
    return bluetoothPeripheral;
}

#pragma mark - 連線＆斷線

- (void)connectPeripheral:(CCCBluetoothPeripheral *)peripheral
                  timeout:(NSTimeInterval)timeoutInterval
               completion:(CCCBluetoothCompletionBlock)completionHandler {
    
    [self.customErrors removeObjectForKey:peripheral.identifier];
    
    if (!peripheral || !peripheral.peripheral) {
        NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Could not connect to device", nil)];
        completionHandler(errorMessage);
        return;
    }
    if ([self.connectedPeripherals containsObject:peripheral]) {
        NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Device connected", nil)];
        completionHandler(errorMessage);
        return;
    }
    
//    //先過濾掉不支援的裝置
//    NSArray *advertisementData = peripheral.advertisementData[CBAdvertisementDataServiceUUIDsKey];
//    NSArray *filteredArray = [advertisementData filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
//        CBUUID *serviceUUID = evaluatedObject;
//        return [serviceUUID.UUIDString isEqualToString:CCLBLEDeviceServiceUUID];
//    }]];
//    if(filteredArray.count == 0){
//        NSError *errorMessage = [self errorWithMessage:PilotLocalizedString(@"Application does not support this device.", nil)];
//        completionHandler(errorMessage);
//        return;
//    }
    
    peripheral.completionHandler = completionHandler;
    
    [self willChangeValueForKey:@"connectedPeripherals"];
    [self.connectedPeripherals addObject:peripheral];
    [self didChangeValueForKey:@"connectedPeripherals"];
    
    [self willChangeValueForKey:@"discoveredPeripherals"];
    [self.discoveredPeripherals removeObject:peripheral];
    [self didChangeValueForKey:@"discoveredPeripherals"];
    
    [self.centerManager connectPeripheral:peripheral.peripheral
                        options:@{CBConnectPeripheralOptionNotifyOnConnectionKey:@YES,
                                  CBConnectPeripheralOptionNotifyOnDisconnectionKey:@YES}];
    
    if (timeoutInterval > 0) {
        // Start connection timeout timer
        [self performSelector:@selector(connectionTimeout:) withObject:peripheral afterDelay:timeoutInterval];
    }
    
}

- (void)connectionTimeout:(CCCBluetoothPeripheral *)peripheral {
    if(peripheral.peripheral.state != CBPeripheralStateConnected) {
        [self connectionTimeoutAndDisconnectPeripheral:peripheral
                                          errorMessage:NSLocalizedString(@"Cannot connect to device", @"")];
    }
}

- (void)disconnectPeripheral:(CCCBluetoothPeripheral *)peripheral
                  completion:(CCCBluetoothCompletionBlock)completionHandler {
    if (!peripheral || !peripheral.peripheral) {
        if (completionHandler) {
            NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Could not disconnect from device", nil)];
            completionHandler(errorMessage);
        }
        
        [self.customErrors removeObjectForKey:peripheral.identifier];
        return;
    }
    if (![self.connectedPeripherals containsObject:peripheral]) {
        if (completionHandler) {
            NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Device disconnected", nil)];
            completionHandler(errorMessage);
        }
        
        [self.customErrors removeObjectForKey:peripheral.identifier];
        return;
    }
    
    peripheral.completionHandler = completionHandler;
    
    [self.centerManager cancelPeripheralConnection:peripheral.peripheral];
}

- (void)connectionTimeoutAndDisconnectPeripheral:(CCCBluetoothPeripheral *)peripheral
                                    errorMessage:(NSString *)errorMessage {
    
    self.customErrors[peripheral.identifier] = [self errorWithMessage:errorMessage];
    
    [self disconnectPeripheral:peripheral
                    completion:peripheral.completionHandler];
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    switch (central.state) {
        case CBManagerStateResetting: {
            NSLog(@"state:CBCentralManagerStateResetting");
            
            [self willChangeValueForKey:@"bluetoothOn"];
            _bluetoothOn = NO;
            [self didChangeValueForKey:@"bluetoothOn"];
            
            [self _stopScan];
            break;
        }
        case CBManagerStateUnsupported: {
            NSLog(@"state:CBCentralManagerStateUnsupported");
            
            [self willChangeValueForKey:@"bluetoothOn"];
            _bluetoothOn = NO;
            [self didChangeValueForKey:@"bluetoothOn"];
            
            [self _stopScan];
            break;
        }
        case CBManagerStateUnauthorized: {
            NSLog(@"state:CBCentralManagerStateUnauthorized");
            
            [self willChangeValueForKey:@"bluetoothOn"];
            _bluetoothOn = NO;
            [self didChangeValueForKey:@"bluetoothOn"];
            
            
            [self _stopScan];
            break;
        }
        case CBManagerStatePoweredOff: {
            NSLog(@"state:CBCentralManagerStatePoweredOff");
            
            [self willChangeValueForKey:@"bluetoothOn"];
            _bluetoothOn = NO;
            [self didChangeValueForKey:@"bluetoothOn"];
            
            
            [self _stopScan];
            break;
        }
        case CBManagerStatePoweredOn: {
            NSLog(@"state:CBCentralManagerStatePoweredOn");
            if(self.isKeepScan){
                [self _startScan];
            }
            
            [self willChangeValueForKey:@"bluetoothOn"];
            _bluetoothOn = YES;
            [self didChangeValueForKey:@"bluetoothOn"];
            
//            [[CCLBLEDevice sharedInstance] reconnectDevice];
            break;
        }
        default:
            NSLog(@"state:CBCentralManagerStateUnknown");
            
            [self willChangeValueForKey:@"bluetoothOn"];
            _bluetoothOn = NO;
            [self didChangeValueForKey:@"bluetoothOn"];
            
            [self _stopScan];
            break;
    }
}

//- (void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary<NSString *, id> *)dict {
//    NSLog(@"%@",dict);
//}

- (void)centralManager:(CBCentralManager *)central
 didDiscoverPeripheral:(CBPeripheral *)peripheral
     advertisementData:(NSDictionary<NSString *, id> *)advertisementData
                  RSSI:(NSNumber *)RSSI {
    
//    NSLog(@"didDiscover:%@", peripheral);
//    NSLog(@"advertisementData:%@", advertisementData);
//    NSLog(@"RSSI:%@", RSSI);
//    NSLog(@"-----------------------------------------");
//    
//    if (peripheral.name) {
//        NSLog(@"掃描到裝置 %@",peripheral.name);
//    }
    
    [self willChangeValueForKey:@"discoveredPeripherals"];
    
    __block CCCBluetoothPeripheral *bluetoothPeripheral;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.identifier == %@", peripheral.identifier];
    NSArray *filteredArray = [self.discoveredPeripherals filteredArrayUsingPredicate:predicate];
    if (filteredArray.count > 0) {
        bluetoothPeripheral = [filteredArray firstObject];
    }
    
    filteredArray = [self.connectedPeripherals filteredArrayUsingPredicate:predicate];
    if (filteredArray.count > 0) {
        bluetoothPeripheral = [filteredArray firstObject];
    }
    
    if (!bluetoothPeripheral) {
        Class aClass = NSClassFromString(self.peripheralClass);
        if (aClass && [aClass isSubclassOfClass:[CCCBluetoothPeripheral class]]) {
            bluetoothPeripheral = [[aClass alloc] init];
        }
        else {
            bluetoothPeripheral = [[CCCBluetoothPeripheral alloc] init];
        }
        [self.discoveredPeripherals addObject:bluetoothPeripheral];
    }
    
    bluetoothPeripheral.deviceName = peripheral.name;
    bluetoothPeripheral.identifier = peripheral.identifier;
    bluetoothPeripheral.peripheral = peripheral;
    bluetoothPeripheral.advertisementData = advertisementData;
    bluetoothPeripheral.rssi = RSSI;
    bluetoothPeripheral.timestamp = [[NSDate date] timeIntervalSince1970];
    
    [bluetoothPeripheral onScanUpdated:RSSI advertisementData:advertisementData];
    
    [self didChangeValueForKey:@"discoveredPeripherals"];
    //NSLog(@"%@", self.discoveredPeripherals);
}



- (void)centralManager:(CBCentralManager *)central
  didConnectPeripheral:(CBPeripheral *)peripheral {
    
    NSLog(@"didConnectPeripheral:%@", peripheral);
    NSLog(@"-----------------------------------------");
    
    CCCBluetoothPeripheral *connectedPeripheral = [self connectedCCCBluetoothPeripheralWithPeripheral:peripheral];
    if (!connectedPeripheral) {
        return;
    }
    
    if (connectedPeripheral.completionHandler) {
        connectedPeripheral.completionHandler(nil);
        connectedPeripheral.completionHandler = nil;
    }
    
}

- (void)    centralManager:(CBCentralManager *)central
didFailToConnectPeripheral:(CBPeripheral *)peripheral
                     error:(NSError *)error {
    
    NSLog(@"didFailToConnectPeripheral:%@", peripheral);
    NSLog(@"error:%@", error);
    NSLog(@"-----------------------------------------");
    
    CCCBluetoothPeripheral *connectedPeripheral = [self connectedCCCBluetoothPeripheralWithPeripheral:peripheral];
    if (!connectedPeripheral) {
        return;
    }
    
    if (connectedPeripheral.completionHandler) {
        connectedPeripheral.completionHandler(error);
        connectedPeripheral.completionHandler = nil;
    }
    
    [self willChangeValueForKey:@"connectedPeripherals"];
    [self.connectedPeripherals removeObject:connectedPeripheral];
    [self didChangeValueForKey:@"connectedPeripherals"];
}

- (void)    centralManager:(CBCentralManager *)central
   didDisconnectPeripheral:(CBPeripheral *)peripheral
                     error:(NSError *)error {
    
    NSLog(@"didDisconnectPeripheral:%@", peripheral);
    NSLog(@"error:%@", error);
    NSLog(@"-----------------------------------------");
    
    [central cancelPeripheralConnection:peripheral];
    
    CCCBluetoothPeripheral *connectedPeripheral = [self connectedCCCBluetoothPeripheralWithPeripheral:peripheral];
    if (!connectedPeripheral) {
        return;
    }
    
    NSError *customError = self.customErrors[connectedPeripheral.identifier];
    
    if (connectedPeripheral.completionHandler) {
        if (error) {
            connectedPeripheral.completionHandler(error);
        }
        else if (customError) {
            connectedPeripheral.completionHandler(customError);
        }
        else {
            connectedPeripheral.completionHandler(nil);
        }
        connectedPeripheral.completionHandler = nil;
    }
    
    NSMutableDictionary *notificationUserInfo = [NSMutableDictionary dictionaryWithDictionary:@{CCCBluetoothManagerDisconnectedPeripheralKey:connectedPeripheral}];
    if (error) {
        notificationUserInfo[CCCBluetoothManagerDisconnectReasonKey] = error;
    }
    else if (customError) {
        notificationUserInfo[CCCBluetoothManagerDisconnectReasonKey] = customError;
    }
    
    [self.customErrors removeObjectForKey:connectedPeripheral.identifier];
    
    [self willChangeValueForKey:@"connectedPeripherals"];
    [self.connectedPeripherals removeObject:connectedPeripheral];
    [self didChangeValueForKey:@"connectedPeripherals"];
    
    NSNotification *notification = [NSNotification notificationWithName:CCCBluetoothManagerPeripheralDidDisconnectNotification object:nil userInfo:notificationUserInfo];
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    
}

#pragma mark - KVO

//方法一
+ (BOOL)automaticallyNotifiesObserversForKey:(NSString *)key {
    if ([key isEqualToString:NSStringFromSelector(@selector(connectedPeripherals))]) {
        return NO;
    }
    
    if ([key isEqualToString:NSStringFromSelector(@selector(discoveredPeripherals))]) {
        return NO;
    }
    
    return [super automaticallyNotifiesObserversForKey:key];
}

////方法二
//+ (BOOL)automaticallyNotifiesObserversOfConnectedPeripheral {
//    return NO;
//}
//
//+ (BOOL)automaticallyNotifiesObserversOfDiscoveredPeripherals {
//    return NO;
//}



#pragma mark - NSError

- (NSError *)errorWithMessage:(NSString *)message {
    NSDictionary *errorMessage = @{NSLocalizedDescriptionKey : message,
                                   NSLocalizedFailureReasonErrorKey : message};
    return [NSError errorWithDomain:CCCBluetoothManagerErrorDomain code:0 userInfo:errorMessage];
}


//#pragma mark - AlertView
//
//- (void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message {
//    if ([UIAlertController class]){
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
//                                                                       message:message
//                                                                preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *okAction = [UIAlertAction actionWithTitle:PilotLocalizedString(@"OK", @"")
//                                                           style:UIAlertActionStyleDefault
//                                                         handler:^(UIAlertAction * action){
//                                                             [alert dismissViewControllerAnimated:YES completion:NULL];
//                                                         }];
//        [alert addAction:okAction];
//        
//        [self.topViewController presentViewController:alert animated:YES completion:NULL];
//        //[[UIApplication sharedApplication].windows.lastObject.rootViewController presentViewController:alert animated:YES completion:NULL];
//    }
//    else{
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Wdeprecated-declarations"
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
//                                                        message:message
//                                                       delegate:nil
//                                              cancelButtonTitle:nil
//                                              otherButtonTitles:PilotLocalizedString(@"OK", @""), nil];
//        [alert show];
//#pragma clang diagnostic pop
//    }
//}
//
//#pragma mark - 尋找topViewController
//
//- (UIViewController *)topViewController {
//    return [self topViewController:[[UIApplication sharedApplication] keyWindow].rootViewController];
//}
//
//- (UIViewController *)topViewController:(UIViewController *)rootViewController {
//    if (rootViewController.presentedViewController == nil) {
//        return rootViewController;
//    }
//    
//    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
//        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
//        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
//        return [self topViewController:lastViewController];
//    }
//    
//    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
//    return [self topViewController:presentedViewController];
//}

@end
