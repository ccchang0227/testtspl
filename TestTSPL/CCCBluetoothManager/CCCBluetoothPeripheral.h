//
//  CCCBluetoothPeripheral.h
//  CCCBluetoothManager
//
//  Created by realtouch on 2017/4/26.
//  Copyright © 2017年 realtouch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

FOUNDATION_EXPORT NSString *const CCCBluetoothManagerErrorDomain;

typedef void (^CCCBluetoothCompletionBlock)(NSError *error);

#pragma mark - CCLBluetoothPeripheral
@interface CCCBluetoothPeripheral : NSObject <NSSecureCoding>

#pragma mark - Property

@property (strong, nonatomic) NSString *deviceName;
@property (strong, nonatomic) NSUUID *identifier;
@property (strong, nonatomic) NSDictionary<NSString*, id> *advertisementData;
@property (strong, nonatomic) NSNumber *rssi;
@property (strong, nonatomic) CBPeripheral *peripheral;
@property (assign, nonatomic) NSTimeInterval timestamp;

@property (strong, nonatomic) NSString *manufacturerName;
@property (strong, nonatomic) NSString *modelNumber;
@property (strong, nonatomic) NSString *serialNumber;
@property (strong, nonatomic) NSString *firmwareRevision;
@property (strong, nonatomic) NSString *hardwareRevision;
@property (strong, nonatomic) NSString *softwareRevision;
@property (strong, nonatomic) NSData *systemID;

@property (strong, nonatomic) CCCBluetoothCompletionBlock completionHandler;

#pragma mark 基本資訊

#pragma mark - Method

- (instancetype)init NS_DESIGNATED_INITIALIZER;

// For override
- (void)onScanUpdated:(NSNumber *)RSSI advertisementData:(NSDictionary<NSString*, id> *)advertisementData;

@end



