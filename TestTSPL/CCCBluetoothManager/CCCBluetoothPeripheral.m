//
//  CCCBluetoothPeripheral.m
//  CCCBluetoothManager
//
//  Created by realtouch on 2017/4/26.
//  Copyright © 2017年 realtouch. All rights reserved.
//

#import "CCCBluetoothPeripheral.h"
#import "CCCBluetoothManager.h"
#import <objc/runtime.h>


NSString *const CCCBluetoothManagerErrorDomain = @"CCLBluetoothManagerErrorDomain";

@interface CCCBluetoothPeripheral () 

//@property (strong, nonatomic) NSUUID *identifier;
//@property (strong, nonatomic) NSDictionary<NSString*, id> *advertisementData;
//@property (strong, nonatomic) NSNumber *rssi;
//@property (strong, nonatomic) CBPeripheral *peripheral;
//@property (assign, nonatomic) NSTimeInterval timeStamp;

@end

@implementation CCCBluetoothPeripheral

+ (BOOL)supportsSecureCoding {
    return YES;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [self init];
    if (self) {
        self.deviceName = [coder decodeObjectOfClass:[NSString class] forKey:@"deviceName"];
        self.identifier = [coder decodeObjectOfClass:[NSUUID class] forKey:@"identifier"];
        self.manufacturerName = [coder decodeObjectOfClass:[NSString class] forKey:@"manufacturerName"];
        self.modelNumber = [coder decodeObjectOfClass:[NSString class] forKey:@"modelNumber"];
        self.serialNumber = [coder decodeObjectOfClass:[NSString class] forKey:@"serialNumber"];
        self.firmwareRevision = [coder decodeObjectOfClass:[NSString class] forKey:@"firmwareRevision"];
        self.hardwareRevision = [coder decodeObjectOfClass:[NSString class] forKey:@"hardwareRevision"];
        self.softwareRevision = [coder decodeObjectOfClass:[NSString class] forKey:@"softwareRevision"];
        self.systemID = [coder decodeObjectOfClass:[NSData class] forKey:@"systemID"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.deviceName forKey:@"deviceName"];
    [coder encodeObject:self.identifier forKey:@"identifier"];
    [coder encodeObject:self.manufacturerName forKey:@"manufacturerName"];
    [coder encodeObject:self.modelNumber forKey:@"modelNumber"];
    [coder encodeObject:self.serialNumber forKey:@"serialNumber"];
    [coder encodeObject:self.firmwareRevision forKey:@"firmwareRevision"];
    [coder encodeObject:self.hardwareRevision forKey:@"hardwareRevision"];
    [coder encodeObject:self.softwareRevision forKey:@"softwareRevision"];
    [coder encodeObject:self.systemID forKey:@"systemID"];
}

#pragma mark -

- (void)onScanUpdated:(NSNumber *)RSSI advertisementData:(NSDictionary<NSString*, id> *)advertisementData {
    
}

#pragma mark - Debug

- (NSString *)description {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    uint count;
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    for (int i = 0; i<count; i++) {
        objc_property_t property = properties[i];
        NSString *name = @(property_getName(property));
        
        //必需過濾掉這4個Key，某些版本似乎會Crash
        NSArray *filters = @[@"superclass", @"description", @"debugDescription", @"hash"];
        if([filters containsObject:name]) {
            continue;
        }
        
        id value = [self valueForKey:name];
        if(value){
            [dictionary setObject:value forKey:name];
        }
        
    }
    
    free(properties);
    
    NSString *debugString = [NSString stringWithFormat:@"<%@: %p> -- %@",[self class],self,dictionary];
    debugString = [debugString stringByReplacingOccurrencesOfString:@"\n" withString:@"\r"];
    return debugString;

}

@end
