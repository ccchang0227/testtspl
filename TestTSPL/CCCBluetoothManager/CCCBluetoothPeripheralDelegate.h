//
//  CCCBluetoothPeripheralDelegate.h
//  TestTSPL
//
//  Created by realtouch's MBP 2019 on 2020/6/5.
//  Copyright © 2020 RealTouchApp Corp. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, CCCBluetoothEventType) {
    CCCBluetoothEventTypeReadRSSI,
    CCCBluetoothEventTypeReadCharacteristic,
    CCCBluetoothEventTypeWriteCharacteristic,
    CCCBluetoothEventTypeNotify,
    CCCBluetoothEventTypeOthers
};

@protocol CCCBluetoothPeripheralEvent <NSObject>

- (void)didReadRSSI:(NSNumber *)RSSI;
- (void)didSearchGattStructure;
- (void)didReadValue:(nullable NSData *)value forCharacteristic:(CBCharacteristic *)characteristic;
- (void)didWriteValueForCharacteristic:(CBCharacteristic *)characteristic;
- (void)didUpdateNotificationState:(BOOL)isNotifying forCharacteristic:(CBCharacteristic *)characteristic;
- (void)isReadytoSendWrite;

- (void)gattErrorWithType:(CCCBluetoothEventType)type error:(NSError *)error;

@end

@interface CCCBluetoothPeripheralDelegate : NSObject <CBPeripheralDelegate>

- (instancetype)init NS_DESIGNATED_INITIALIZER;

- (void)configureGatt:(CBPeripheral *)peripheral delegate:(nullable id<CCCBluetoothPeripheralEvent>)delegate;
- (nullable CBService *)findServiceWithUUID:(CBUUID *)uuid;
- (nullable CBCharacteristic *)findCharacteristicWithUUID:(CBUUID *)uuid inService:(nullable CBService *)service;
- (nullable CBCharacteristic *)findCharacteristicWithUUID:(CBUUID *)uuid inServiceUUID:(CBUUID *)serviceUUID;

+ (nullable NSString *)toUtf8String:(nullable NSData *)data;

@end

NS_ASSUME_NONNULL_END
