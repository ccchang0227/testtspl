//
//  CCCBluetoothPeripheralDelegate.m
//  TestTSPL
//
//  Created by realtouch's MBP 2019 on 2020/6/5.
//  Copyright © 2020 RealTouchApp Corp. Ltd. All rights reserved.
//

#import "CCCBluetoothPeripheralDelegate.h"


@interface CCCBluetoothPeripheralDelegate()

@property (weak, nonatomic) id<CCCBluetoothPeripheralEvent> _Nullable delegate;
@property (strong, nonatomic) CBPeripheral * _Nullable peripheral;

@property (nonatomic) NSInteger discoverProgress;

@end

@implementation CCCBluetoothPeripheralDelegate

+ (nullable NSString *)toUtf8String:(nullable NSData *)data {
    if (!data) {
        return nil;
    }
    
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.discoverProgress = 0;
    }
    return self;
}

- (void)setPeripheral:(CBPeripheral *)peripheral {
    if (peripheral != _peripheral) {
        if (_peripheral) {
            _peripheral.delegate = nil;
        }
        
        _peripheral = peripheral;
    }
}

#pragma mark -

- (void)configureGatt:(CBPeripheral *)peripheral delegate:(nullable id<CCCBluetoothPeripheralEvent>)delegate {
    self.delegate = delegate;
    self.peripheral = peripheral;
    
    if (!peripheral) {
        return;
    }
    if (peripheral.state != CBPeripheralStateConnected) {
        return;
    }
    
    peripheral.delegate = self;
    
    self.discoverProgress = 0;
    if (peripheral.services) {
        [self didDiscoverServices:peripheral.services withPeripheral:peripheral];
    }
    else {
        [peripheral discoverServices:nil];
    }
}

- (nullable CBService *)findServiceWithUUID:(CBUUID *)uuid {
    if (!self.peripheral) {
        return nil;
    }
    if (!self.peripheral.services || self.peripheral.services.count == 0) {
        return nil;
    }
    
    return [self.peripheral.services filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"UUID=%@", uuid]].firstObject;
}

- (nullable CBCharacteristic *)findCharacteristicWithUUID:(CBUUID *)uuid inService:(nullable CBService *)service {
    if (!service) {
        return nil;
    }
    if (!service.characteristics || service.characteristics.count == 0) {
        return nil;
    }
    
    return [service.characteristics filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"UUID=%@", uuid]].firstObject;
}

- (nullable CBCharacteristic *)findCharacteristicWithUUID:(CBUUID *)uuid inServiceUUID:(CBUUID *)serviceUUID {
    return [self findCharacteristicWithUUID:uuid inService:[self findServiceWithUUID:serviceUUID]];
}

#pragma mark -

- (void)didDiscoverServices:(NSArray<CBService *> *)services withPeripheral:(CBPeripheral *)peripheral {
    for (CBService *service in services) {
        if (service.characteristics) {
            [self didDiscoverCharacteristics:service.characteristics withPeripheral:peripheral];
        }
        else {
            self.discoverProgress ++;
            [peripheral discoverCharacteristics:nil forService:service];
        }
    }
    
    if (self.discoverProgress == 0) {
        [self gattStructureSearchComplete];
    }
}

- (void)didDiscoverCharacteristics:(NSArray<CBCharacteristic *> *)characteristics withPeripheral:(CBPeripheral *)peripheral {
    for (CBCharacteristic *characteristic in characteristics) {
        if (!characteristic.descriptors) {
            self.discoverProgress ++;
            [peripheral discoverDescriptorsForCharacteristic:characteristic];
        }
    }
}

- (void)gattStructureSearchComplete {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSearchGattStructure)]) {
        [self.delegate didSearchGattStructure];
    }
}

#pragma mark - CBPeripheralDelegate

- (void)peripheral:(CBPeripheral *)peripheral didReadRSSI:(NSNumber *)RSSI error:(NSError *)error {
    if (error) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(gattErrorWithType:error:)]) {
            [self.delegate gattErrorWithType:CCCBluetoothEventTypeReadRSSI error:error];
        }
        return;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didReadRSSI:)]) {
        [self.delegate didReadRSSI:RSSI];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    if (!peripheral.services) {
        [self gattStructureSearchComplete];
        return;
    }
    
    [self didDiscoverServices:peripheral.services withPeripheral:peripheral];
    
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    self.discoverProgress --;
    
    if (!service.characteristics) {
        if (self.discoverProgress == 0) {
            [self gattStructureSearchComplete];
        }
        return;
    }
    
    [self didDiscoverCharacteristics:service.characteristics withPeripheral:peripheral];
    
    if (self.discoverProgress == 0) {
        [self gattStructureSearchComplete];
    }
    
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    self.discoverProgress --;
    
    if (!characteristic.descriptors) {
        if (self.discoverProgress == 0) {
            [self gattStructureSearchComplete];
        }
        return;
    }
    
    if (self.discoverProgress == 0) {
        [self gattStructureSearchComplete];
    }
    
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(NSError *)error {
    
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    
    if (error) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(gattErrorWithType:error:)]) {
            [self.delegate gattErrorWithType:CCCBluetoothEventTypeReadCharacteristic error:error];
        }
    }
    else {
        if (self.delegate && [self.delegate respondsToSelector:@selector(didReadValue:forCharacteristic:)]) {
            [self.delegate didReadValue:characteristic.value forCharacteristic:characteristic];
        }
    }
    
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    
    if (error) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(gattErrorWithType:error:)]) {
            [self.delegate gattErrorWithType:CCCBluetoothEventTypeWriteCharacteristic error:error];
        }
    }
    else {
        if (self.delegate && [self.delegate respondsToSelector:@selector(didWriteValueForCharacteristic:)]) {
            [self.delegate didWriteValueForCharacteristic:characteristic];
        }
    }
    
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    
    if (error) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(gattErrorWithType:error:)]) {
            [self.delegate gattErrorWithType:CCCBluetoothEventTypeNotify error:error];
        }
    }
    else {
        if (self.delegate && [self.delegate respondsToSelector:@selector(didUpdateNotificationState:forCharacteristic:)]) {
            [self.delegate didUpdateNotificationState:characteristic.isNotifying forCharacteristic:characteristic];
        }
    }
    
}

- (void)peripheralIsReadyToSendWriteWithoutResponse:(CBPeripheral *)peripheral {
    if (self.delegate && [self.delegate respondsToSelector:@selector(isReadytoSendWrite)]) {
        [self.delegate isReadytoSendWrite];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didModifyServices:(NSArray<CBService *> *)invalidatedServices {
    
}

@end
