//
//  CCCBluetoothGprinter.h
//  TestTSPL
//
//  Created by realtouch's MBP 2019 on 2020/6/9.
//  Copyright © 2020 RealTouchApp Corp. Ltd. All rights reserved.
//

#import <CoreBluetooth/CoreBluetooth.h>
#import "CCCGprinterBtOperation.h"
#import "CCCGpTsplCommand.h"

#define CCCBtGpMainService           @"49535343-FE7D-4AE5-8FA9-9FAFD205E455"
#define CCCBtGpWriteCharacteristic   @"49535343-8841-43F4-A8D4-ECBE34729BB3"
#define CCCBtGpReadCharacteristic    @"49535343-1E4D-4BD9-BA61-23C647249616"

typedef void(^CCCGprinterBtOperationHandler)(id response, NSString *string, NSError *error);

typedef NS_OPTIONS(NSInteger, CCCBluetoothGprinterStatus) {
    /// 正常
    CCCBluetoothGprinterStatusNormal = 0x00,
    /// 打印机未关
    CCCBluetoothGprinterStatusHeadOpened = 0x01,
    /// 卡纸
    CCCBluetoothGprinterStatusPaperJam = 0x02,
    /// 缺纸
    CCCBluetoothGprinterStatusOutOfPaper = 0x04,
    /// 无碳带
    CCCBluetoothGprinterStatusOutOfRibbon = 0x08,
    /// 暂停打印
    CCCBluetoothGprinterStatusPaused = 0x10,
    /// 打印中
    CCCBluetoothGprinterStatusPrinting = 0x20,
    /// 机壳未关
    CCCBluetoothGprinterStatusCoverOpened = 0x40,
    /// 其他错误
    CCCBluetoothGprinterStatusOtherError = 0x80
};
typedef void(^GetStatusHandler)(CCCBluetoothGprinterStatus status, NSError *error);

@interface CCCBluetoothGprinter : NSObject <CBPeripheralDelegate>

@property (strong, nonatomic) NSString *deviceName;
@property (strong, nonatomic) NSUUID *identifier;
@property (strong, nonatomic) NSDictionary<NSString*, id> *advertisementData;
@property (strong, nonatomic) NSNumber *rssi;
@property (strong, nonatomic) CBPeripheral *peripheral;
@property (assign, nonatomic) NSTimeInterval timestamp;

@property (strong, nonatomic) NSString *manufacturerName;
@property (strong, nonatomic) NSString *modelNumber;
@property (strong, nonatomic) NSString *serialNumber;
@property (strong, nonatomic) NSString *firmwareRevision;
@property (strong, nonatomic) NSString *hardwareRevision;
@property (strong, nonatomic) NSString *softwareRevision;
@property (strong, nonatomic) NSData *systemID;

@property (copy, nonatomic) void(^completionHandler)(NSError *error);

@property (readonly, strong, nonatomic) NSOperationQueue *commandQueue;
- (void)clearCommandQueue;

/// 讀取GATT結構 (連線後呼叫)
- (void)configureGatt;

/// 讀取
- (void)readValueForCharacteristic:(CBUUID *)characteristicUUID
                           service:(CBUUID *)serviceUUID
                           timeout:(NSTimeInterval)timeoutInterval
                        completion:(CCCGprinterBtOperationHandler)completion;
/// 寫入
- (void)writeValue:(NSData *)value
 forCharacteristic:(CBUUID *)characteristicUUID
           service:(CBUUID *)serviceUUID
           timeout:(NSTimeInterval)timeoutInterval
        completion:(CCCGprinterBtOperationHandler)completion;
/// 設定通知
- (void)setNotify:(BOOL)enabled
forCharacteristic:(CBUUID *)characteristicUUID
          service:(CBUUID *)serviceUUID
          timeout:(NSTimeInterval)timeoutInterval
       completion:(CCCGprinterBtOperationHandler)completion;

#pragma mark -

/// 非同步讀取裝置資訊 (serial number, model number...)
- (void)readDeviceInformation;
/// 開啟通知
- (BOOL)enableNotification;
/// 取得印表機狀態
- (void)getPrinterStatusWithHandler:(GetStatusHandler)handler;
/// 取得印表機型號 (不是實際的型號，是韌體使用的型號，e.g. MODEL:GP-2120)
- (void)getPrinterModelWithHandler:(void(^)(NSString *model, NSError *error))handler;

@end
