//
//  CCCBluetoothGprinter.m
//  TestTSPL
//
//  Created by realtouch's MBP 2019 on 2020/6/9.
//  Copyright © 2020 RealTouchApp Corp. Ltd. All rights reserved.
//

#import "CCCBluetoothGprinter.h"
#import <objc/runtime.h>

#define DeviceInformationService    @"0000180a-0000-1000-8000-00805f9b34fb"
#define ManufacturerName            @"00002A29-0000-1000-8000-00805f9b34fb"
#define ModelNumber                 @"00002a24-0000-1000-8000-00805f9b34fb"
#define SerialNumber                @"00002a25-0000-1000-8000-00805f9b34fb"
#define HardwareRevision            @"00002a27-0000-1000-8000-00805f9b34fb"
#define FirmwareRevision            @"00002a26-0000-1000-8000-00805f9b34fb"
#define SoftwareRevision            @"00002a28-0000-1000-8000-00805f9b34fb"
#define SystemID                    @"00002a23-0000-1000-8000-00805f9b34fb"
// IEEE 11073-20601 Regulatory Certification Data List
#define IEEE_RCDL                   @"00002a2a-0000-1000-8000-00805f9b34fb"
#define PnpID                       @"00002a50-0000-1000-8000-00805f9b34fb"

@interface CCCBluetoothGprinter ()

@property (nonatomic) NSInteger discoverProgress;

@property (copy, nonatomic) GetStatusHandler getStatusHandler;
@property (copy, nonatomic) void(^getModelHandler)(NSString *, NSError *);

@end

@implementation CCCBluetoothGprinter
@synthesize commandQueue = _commandQueue;

#pragma mark - Debug

- (NSString *)description {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    uint count;
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    for (int i = 0; i<count; i++) {
        objc_property_t property = properties[i];
        NSString *name = @(property_getName(property));
        
        //必需過濾掉這4個Key，某些版本似乎會Crash
        NSArray *filters = @[@"superclass", @"description", @"debugDescription", @"hash"];
        if([filters containsObject:name]) {
            continue;
        }
        
        id value = [self valueForKey:name];
        if(value){
            [dictionary setObject:value forKey:name];
        }
        
    }
    
    free(properties);
    
    NSString *debugString = [NSString stringWithFormat:@"<%@: %p> -- %@",[self class],self,dictionary];
    debugString = [debugString stringByReplacingOccurrencesOfString:@"\n" withString:@"\r"];
    return debugString;

}

#pragma mark -

- (void)configureGatt {
    if (!self.peripheral) {
        return;
    }
    if (self.peripheral.state != CBPeripheralStateConnected) {
        return;
    }
    
    self.peripheral.delegate = self;
    
    self.discoverProgress = 0;
    if (self.peripheral.services) {
        [self didDiscoverServices:self.peripheral.services];
    }
    else {
        [self.peripheral discoverServices:nil];
    }
}

- (CBService *)findServiceWithUUID:(CBUUID *)uuid {
    if (!self.peripheral) {
        return nil;
    }
    if (!self.peripheral.services || self.peripheral.services.count == 0) {
        return nil;
    }
    
    return [self.peripheral.services filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"UUID=%@", uuid]].firstObject;
}

- (CBCharacteristic *)findCharacteristicWithUUID:(CBUUID *)uuid inService:(CBService *)service {
    if (!service) {
        return nil;
    }
    if (!service.characteristics || service.characteristics.count == 0) {
        return nil;
    }
    
    return [service.characteristics filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"UUID=%@", uuid]].firstObject;
}

- (CBCharacteristic *)findCharacteristicWithUUID:(CBUUID *)uuid inServiceUUID:(CBUUID *)serviceUUID {
    return [self findCharacteristicWithUUID:uuid inService:[self findServiceWithUUID:serviceUUID]];
}

#pragma mark -

+ (NSString *)toAsciiString:(NSData *)data {
    if (!data) {
        return nil;
    }
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

- (void)didDiscoverServices:(NSArray<CBService *> *)services {
    for (CBService *service in services) {
        if (service.characteristics) {
            [self didDiscoverCharacteristics:service.characteristics];
        }
        else {
            self.discoverProgress ++;
            [self.peripheral discoverCharacteristics:nil forService:service];
        }
    }
    
    if (self.discoverProgress == 0) {
        [self gattStructureSearchComplete];
    }
}

- (void)didDiscoverCharacteristics:(NSArray<CBCharacteristic *> *)characteristics {
    for (CBCharacteristic *characteristic in characteristics) {
        if (!characteristic.descriptors) {
            self.discoverProgress ++;
            [self.peripheral discoverDescriptorsForCharacteristic:characteristic];
        }
    }
}

- (void)gattStructureSearchComplete {
    if (self.completionHandler) {
        self.completionHandler(nil);
    }
    self.completionHandler = nil;
}

#pragma mark - CBPeripheralDelegate

- (void)peripheral:(CBPeripheral *)peripheral didReadRSSI:(NSNumber *)RSSI error:(NSError *)error {
    if (error) {
        return;
    }
    
    
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    if (!peripheral.services) {
        [self gattStructureSearchComplete];
        return;
    }
    
    [self didDiscoverServices:peripheral.services];
    
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    self.discoverProgress --;
    
    if (!service.characteristics) {
        if (self.discoverProgress == 0) {
            [self gattStructureSearchComplete];
        }
        return;
    }
    
    [self didDiscoverCharacteristics:service.characteristics];
    
    if (self.discoverProgress == 0) {
        [self gattStructureSearchComplete];
    }
    
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    self.discoverProgress --;
    
    if (!characteristic.descriptors) {
        if (self.discoverProgress == 0) {
            [self gattStructureSearchComplete];
        }
        return;
    }
    
    if (self.discoverProgress == 0) {
        [self gattStructureSearchComplete];
    }
    
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(NSError *)error {
    
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    
    CCCGprinterBtOperation *operation = self.commandQueue.operations.firstObject;
    if (operation && ![operation isKindOfClass:[CCCGprinterBtOperation class]]) {
        operation = nil;
    }
    if (operation && operation.type != CCCGprinterBtOperationTypeRead) {
        operation = nil;
    }
    
    if (error) {
        if (operation) {
            [operation completeWithResponse:error];
        }
    }
    else {
        if (operation) {
            [operation completeWithResponse:characteristic.value];
        }
    }
    
    if (!operation) {
        if (self.getStatusHandler) {
            NSData *value = characteristic.value;
            CCCBluetoothGprinterStatus status = CCCBluetoothGprinterStatusOtherError;
            if (value) {
                const uint8_t *bytes = value.bytes;
                status = bytes[0];
            }
            self.getStatusHandler(status, error);
            
            self.getStatusHandler = nil;
        }
        else if (self.getModelHandler) {
            NSData *value = characteristic.value;
            NSString *model = nil;
            if (value) {
                model = [[NSString alloc] initWithData:value encoding:NSASCIIStringEncoding];
            }
            
            self.getModelHandler(model, error);
            self.getModelHandler = nil;
        }
        
    }
    
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    
    CCCGprinterBtOperation *operation = self.commandQueue.operations.firstObject;
    if (operation && ![operation isKindOfClass:[CCCGprinterBtOperation class]]) {
        operation = nil;
    }
    if (operation && operation.type != CCCGprinterBtOperationTypeWrite) {
        operation = nil;
    }
    
    if (error) {
        if (operation) {
            [operation completeWithResponse:error];
        }
    }
    else {
        if (operation) {
            [operation complete];
        }
    }
    
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    
    CCCGprinterBtOperation *operation = self.commandQueue.operations.firstObject;
    if (operation && ![operation isKindOfClass:[CCCGprinterBtOperation class]]) {
        operation = nil;
    }
    if (operation && operation.type != CCCGprinterBtOperationTypeNotify) {
        operation = nil;
    }
    
    if (error) {
        if (operation) {
            [operation completeWithResponse:error];
        }
    }
    else {
        if (operation) {
            [operation completeWithResponse:@(characteristic.isNotifying)];
        }
    }
    
}

- (void)peripheralIsReadyToSendWriteWithoutResponse:(CBPeripheral *)peripheral {
    CCCGprinterBtOperation *operation = self.commandQueue.operations.firstObject;
    if (operation && ![operation isKindOfClass:[CCCGprinterBtOperation class]]) {
        operation = nil;
    }
    if (operation && operation.type != CCCGprinterBtOperationTypeWrite) {
        operation = nil;
    }
    
    if (operation) {
        [operation complete];
    }
    
}

- (void)peripheral:(CBPeripheral *)peripheral didModifyServices:(NSArray<CBService *> *)invalidatedServices {
    
}

#pragma mark -

- (NSOperationQueue *)commandQueue {
    if (_commandQueue == nil) {
        _commandQueue = [[NSOperationQueue alloc] init];
        _commandQueue.maxConcurrentOperationCount = 1;
        [_commandQueue setSuspended:NO];
    }
    return _commandQueue;
}

- (void)clearCommandQueue {
    [self.commandQueue cancelAllOperations];
}

- (void)addOperation:(CCCGprinterBtOperation *)operation
             timeout:(NSTimeInterval)timeoutInterval
          completion:(CCCGprinterBtOperationHandler)completion {
    __weak CCCGprinterBtOperation *weakOp = operation;
    operation.completionBlock = ^{
        if (!weakOp) {
            return;
        }
        
        __strong CCCGprinterBtOperation *strongOp = weakOp;
        if (strongOp.isCancelled) {
            return;
        }
        
        if (strongOp.response && [strongOp.response isKindOfClass:[NSError class]]) {
            // Error Occurred
            if (completion) {
                completion(nil, nil, (NSError *)strongOp.response);
            }
            return;
        }
        
        NSData *data = nil;
        if (strongOp.response && [strongOp.response isKindOfClass:[NSData class]]) {
            data = (NSData *)strongOp.response;
        }
        NSString *string = [CCCBluetoothGprinter toAsciiString:data];
        if (completion) {
            completion(data, string, nil);
        }
        
    };
    
    operation.timeoutInterval = timeoutInterval;
    [self.commandQueue addOperation:operation];
}

- (void)readValueForCharacteristic:(CBUUID *)characteristicUUID
                           service:(CBUUID *)serviceUUID
                           timeout:(NSTimeInterval)timeoutInterval
                        completion:(CCCGprinterBtOperationHandler)completion {
    CBPeripheral *cbPeripheral = self.peripheral;
    if (!cbPeripheral) {
        if (completion) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"No peripheral", nil),
                                       NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"No peripheral", nil)};
            NSError *error = [NSError errorWithDomain:@"BtSerialOperationError" code:-8764 userInfo:userInfo];
            completion(nil, nil, error);
        }
        return;
    }
    CBCharacteristic *characteristic = [self findCharacteristicWithUUID:characteristicUUID inServiceUUID:serviceUUID];
    if (!characteristic) {
        if (completion) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"Characteristic not found", nil),
                                       NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Characteristic not found", nil)};
            NSError *error = [NSError errorWithDomain:@"BtSerialOperationError" code:-8765 userInfo:userInfo];
            completion(nil, nil, error);
        }
        return;
    }
    
    CCCGprinterBtOperation *operation = [[CCCGprinterBtOperation alloc] initWithType:CCCGprinterBtOperationTypeRead
                                                                          peripheral:cbPeripheral
                                                                      characteristic:characteristic];
    [self addOperation:operation timeout:timeoutInterval completion:completion];
}

- (void)writeValue:(NSData *)value
 forCharacteristic:(CBUUID *)characteristicUUID
           service:(CBUUID *)serviceUUID
           timeout:(NSTimeInterval)timeoutInterval
        completion:(CCCGprinterBtOperationHandler)completion {
    CBPeripheral *cbPeripheral = self.peripheral;
    if (!cbPeripheral) {
        if (completion) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"No peripheral", nil),
                                       NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"No peripheral", nil)};
            NSError *error = [NSError errorWithDomain:@"BtSerialOperationError" code:-8764 userInfo:userInfo];
            completion(nil, nil, error);
        }
        return;
    }
    CBCharacteristic *characteristic = [self findCharacteristicWithUUID:characteristicUUID inServiceUUID:serviceUUID];
    if (!characteristic) {
        if (completion) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"Characteristic not found", nil),
                                       NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Characteristic not found", nil)};
            NSError *error = [NSError errorWithDomain:@"BtSerialOperationError" code:-8765 userInfo:userInfo];
            completion(nil, nil, error);
        }
        return;
    }
    
    CBCharacteristicWriteType writeType = CBCharacteristicWriteWithResponse;
    if ((characteristic.properties & CBCharacteristicWriteWithResponse) == 0) {
        writeType = CBCharacteristicWriteWithoutResponse;
    }
    
    CCCGprinterBtOperation *operation = [[CCCGprinterBtOperation alloc] initWithType:CCCGprinterBtOperationTypeWrite
                                                                          peripheral:cbPeripheral
                                                                      characteristic:characteristic
                                                                               value:value
                                                                           writeType:writeType];
    [self addOperation:operation timeout:timeoutInterval completion:completion];
}

- (void)setNotify:(BOOL)enabled
forCharacteristic:(CBUUID *)characteristicUUID
          service:(CBUUID *)serviceUUID
          timeout:(NSTimeInterval)timeoutInterval
       completion:(CCCGprinterBtOperationHandler)completion {
    CBPeripheral *cbPeripheral = self.peripheral;
    if (!cbPeripheral) {
        if (completion) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"No peripheral", nil),
                                       NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"No peripheral", nil)};
            NSError *error = [NSError errorWithDomain:@"BtSerialOperationError" code:-8764 userInfo:userInfo];
            completion(nil, nil, error);
        }
        return;
    }
    CBCharacteristic *characteristic = [self findCharacteristicWithUUID:characteristicUUID inServiceUUID:serviceUUID];
    if (!characteristic) {
        if (completion) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"Characteristic not found", nil),
                                       NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Characteristic not found", nil)};
            NSError *error = [NSError errorWithDomain:@"BtSerialOperationError" code:-8765 userInfo:userInfo];
            completion(nil, nil, error);
        }
        return;
    }
    
    CCCGprinterBtOperation *operation = [[CCCGprinterBtOperation alloc] initWithType:CCCGprinterBtOperationTypeNotify
                                                                          peripheral:cbPeripheral
                                                                      characteristic:characteristic
                                                                               value:@(enabled)
                                                                           writeType:CBCharacteristicWriteWithResponse];
    [self addOperation:operation timeout:timeoutInterval completion:completion];
}

#pragma mark -

- (void)readDeviceInformation {
    [self readValueForCharacteristic:[CBUUID UUIDWithString:ManufacturerName] service:[CBUUID UUIDWithString:DeviceInformationService] timeout:1.0 completion:^(id response, NSString *string, NSError *error) {
        
        self.manufacturerName = string;
        NSLog(@"Manufacturer name: %@", string);
        
    }];
    
    [self readValueForCharacteristic:[CBUUID UUIDWithString:ModelNumber] service:[CBUUID UUIDWithString:DeviceInformationService] timeout:1.0 completion:^(id response, NSString *string, NSError *error) {
        
        self.modelNumber = string;
        NSLog(@"Model number: %@", string);
        
    }];
    
    [self readValueForCharacteristic:[CBUUID UUIDWithString:SerialNumber] service:[CBUUID UUIDWithString:DeviceInformationService] timeout:1.0 completion:^(id response, NSString *string, NSError *error) {
        
        self.serialNumber = string;
        NSLog(@"Serial number: %@", string);
        
    }];
    
    [self readValueForCharacteristic:[CBUUID UUIDWithString:HardwareRevision] service:[CBUUID UUIDWithString:DeviceInformationService] timeout:1.0 completion:^(id response, NSString *string, NSError *error) {
        
        self.hardwareRevision = string;
        NSLog(@"Hardware revision: %@", string);
        
    }];
    
    [self readValueForCharacteristic:[CBUUID UUIDWithString:FirmwareRevision] service:[CBUUID UUIDWithString:DeviceInformationService] timeout:1.0 completion:^(id response, NSString *string, NSError *error) {
        
        self.firmwareRevision = string;
        NSLog(@"Firmware revision: %@", string);
        
    }];
    
    [self readValueForCharacteristic:[CBUUID UUIDWithString:SoftwareRevision] service:[CBUUID UUIDWithString:DeviceInformationService] timeout:1.0 completion:^(id response, NSString *string, NSError *error) {
        
        self.softwareRevision = string;
        NSLog(@"Software revision: %@", string);
        
    }];
    
    [self readValueForCharacteristic:[CBUUID UUIDWithString:SystemID] service:[CBUUID UUIDWithString:DeviceInformationService] timeout:1.0 completion:^(id response, NSString *string, NSError *error) {
        
        if (response && [response isKindOfClass:[NSData class]]) {
            self.systemID = (NSData *)response;
        }
        
    }];
    
}

- (BOOL)enableNotification {
    CBCharacteristic *characteristic = [self findCharacteristicWithUUID:[CBUUID UUIDWithString:CCCBtGpReadCharacteristic]
                                                          inServiceUUID:[CBUUID UUIDWithString:CCCBtGpMainService]];
    if (!characteristic) {
        return NO;
    }
    if (characteristic.isNotifying) {
        return YES;
    }
    
    dispatch_semaphore_t signal = dispatch_semaphore_create(0);
    __block BOOL isNotifying = NO;
    
    [self setNotify:YES forCharacteristic:[CBUUID UUIDWithString:CCCBtGpReadCharacteristic] service:[CBUUID UUIDWithString:CCCBtGpMainService] timeout:1.0 completion:^(id response, NSString *string, NSError *error) {
        
        if (response) {
            isNotifying = [response boolValue];
        }
        dispatch_semaphore_signal(signal);
        
    }];
    
    dispatch_semaphore_wait(signal, dispatch_time(DISPATCH_TIME_NOW, 1.3 * NSEC_PER_SEC));
    
    return isNotifying;
}

- (void)getPrinterStatusWithHandler:(GetStatusHandler)handler {
    if (![self enableNotification]) {
        if (handler) {
            handler(CCCBluetoothGprinterStatusOtherError, nil);
        }
        return;
    }
    
    self.getStatusHandler = handler;
    
    [self writeValue:[CCCGpTsplCommand getPrinterStatus] forCharacteristic:[CBUUID UUIDWithString:CCCBtGpWriteCharacteristic] service:[CBUUID UUIDWithString:CCCBtGpMainService] timeout:2.0 completion:^(id response, NSString *string, NSError *error) {
        
        if (error) {
            self.getStatusHandler = nil;
            
            if (handler) {
                handler(CCCBluetoothGprinterStatusOtherError, error);
            }
        }
        
    }];
}

- (void)getPrinterModelWithHandler:(void(^)(NSString *model, NSError *error))handler {
    if (![self enableNotification]) {
        if (handler) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"Cannot enable notification", nil),
                                       NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Cannot enable notification", nil)};
            NSError *error = [NSError errorWithDomain:@"BtSerialOperationError" code:-8766 userInfo:userInfo];
            handler(nil, error);
        }
        return;
    }
    
    self.getModelHandler = handler;
    
    [self writeValue:[CCCGpTsplCommand getPrinterModel] forCharacteristic:[CBUUID UUIDWithString:CCCBtGpWriteCharacteristic] service:[CBUUID UUIDWithString:CCCBtGpMainService] timeout:2.0 completion:^(id response, NSString *string, NSError *error) {
        
        if (error) {
            self.getModelHandler = nil;
            
            if (handler) {
                handler(nil, error);
            }
        }
        
    }];
}

@end
