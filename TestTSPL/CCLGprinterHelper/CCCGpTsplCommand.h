//
//  CCCGpTsplCommand.h
//  TestTSPL
//
//  Created by realtouch's MBP 2019 on 2020/6/8.
//  Copyright © 2020 RealTouchApp Corp. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, CCCGpTsplCommandSpeed) {
    /// 38mm/s
    CCCGpTsplCommandSpeed1_5,
    /// 50mm/s
    CCCGpTsplCommandSpeed2,
    /// 72mm/s
    CCCGpTsplCommandSpeed3,
    /// 100mm/s
    CCCGpTsplCommandSpeed4
};

typedef NS_ENUM(NSInteger, CCCGpTsplCommandDirection) {
    /// 正常方向
    CCCGpTsplCommandDirectionNormal = 0,
    /// 反方向
    CCCGpTsplCommandDirectionReverse = 1
};

typedef NS_ENUM(NSInteger, CCCGpTsplCommandMirrorType) {
    /// 正常模式
    CCCGpTsplCommandMirrorTypeNormal = 0,
    /// 鏡向模式
    CCCGpTsplCommandMirrorTypeMirror = 1
};

typedef NS_ENUM(NSInteger, CCCGpTsplCommandBitmapMode) {
    CCCGpTsplCommandBitmapModeOverwrite = 0,
    CCCGpTsplCommandBitmapModeOr = 1,
    CCCGpTsplCommandBitmapModeXor = 2
};

/** 標籤機用指令 */
@interface CCCGpTsplCommand : NSObject

/// 預設
+ (NSData *)defaultSettings;

/// 測試頁
+ (NSData *)selfTest;

/// 清除圖像緩衝區
+ (NSData *)cls;

/// 設定列印速度
/// @param speed 列印速度
+ (NSData *)printSpeed:(CCCGpTsplCommandSpeed)speed;

/// 設定標籤的參考起點
/// @param x 水平位置 (以點為單位，與印表機的DPI有關)
/// @param y 垂直位置 (以點為單位，與印表機的DPI有關)
+ (NSData *)referencePositionX:(NSInteger)x positionY:(NSInteger)y;

/// 設定印表機定位完成後再走紙多少毫米
/// @param offset 走紙距離 (單位mm)
+ (NSData *)offsetWithDistance:(NSInteger)offset;

/// 設定列印濃度
/// @param density 濃度 (0~15)
+ (NSData *)printDensity:(NSInteger)density;

/// 設定標籤紙大小
/// @param width 寬度 (單位mm)
/// @param height 長度 (單位mm)
+ (NSData *)printAreaSizeWithWidth:(NSInteger)width height:(NSInteger)height;

/// 設定標籤紙間距
/// @param distance 兩張標籤紙中間的垂直距離 (單位mm)
/// @param offset  垂直間距的偏移 (單位mm)
+ (NSData *)gapWithDistance:(NSInteger)distance offset:(NSInteger)offset;

/// 設定列印的方向和鏡像
/// @param direction 列印方向
/// @param mirror 鏡像與否
+ (NSData *)printDirection:(CCCGpTsplCommandDirection)direction mirror:(CCCGpTsplCommandMirrorType)mirror;

/// 設定列印數量和複本數量，並進行列印
/// @param sets 列印數量
/// @param copies 複本數量
+ (NSData *)printWithSets:(NSInteger)sets copies:(NSInteger)copies;

/// 計算適合標籤紙的數值
/// @param width 寬度 (單位mm)
/// @param dpi 印表機的DPI
+ (CGFloat)preferredWidthFromWidth:(CGFloat)width dpi:(CGFloat)dpi;

/// 設定列印圖片
/// @param image 圖片
/// @param mode 圖片列印模式
+ (NSData *)printImage:(UIImage *)image mode:(CCCGpTsplCommandBitmapMode)mode;

//+ (NSData *)testImage;

/// 查詢印表機狀態
+ (NSData *)getPrinterStatus;

/// 查詢印表機型號
+ (NSData *)getPrinterModel;

/// 開錢箱 (要看印表機有無支援，未測試)
+ (NSData *)openCashDrawer;

/// 控制进纸的距离 (以點為單位，與印表機的DPI有關)
+ (NSData *)feedLine:(NSInteger)line;

/// 控制退纸的距离 (以點為單位，與印表機的DPI有關)
+ (NSData *)backFeed:(NSInteger)line;

/// 控制打印机进一张纸
+ (NSData *)formFeed;

/// 控制蜂鸣器的频率 (要看印表機有無支援，未測試)
/// @param level 10阶的声音 (0~9)
/// @param interval 每阶声音的长短 (單位應該是ms)
+ (NSData *)soundWith:(NSInteger)level interval:(NSInteger)interval;

/// 控制蜂鸣器收到该指令时会发出一声响 (未測試)
+ (NSData *)beep;

@end
