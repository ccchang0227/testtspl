//
//  CCCGpTsplCommand.m
//  TestTSPL
//
//  Created by realtouch's MBP 2019 on 2020/6/8.
//  Copyright © 2020 RealTouchApp Corp. Ltd. All rights reserved.
//

#import "CCCGpTsplCommand.h"

typedef struct {
    /// Alpha component
    UInt8 alpha;
    /// Red component
    UInt8 red;
    /// Green component
    UInt8 green;
    /// Blue component
    UInt8 blue;
} ARGBPixel;

@implementation CCCGpTsplCommand

+ (NSData *)defaultSettings {
    NSMutableData *data = [[NSMutableData alloc] init];
    [data appendData:[self cls]];
    [data appendData:[self printSpeed:CCCGpTsplCommandSpeed4]];
    [data appendData:[self referencePositionX:0 positionY:0]];
    [data appendData:[self offsetWithDistance:0]];
    [data appendData:[self printDensity:8]];
    
    NSString *others = @"SET TEAR ON\r\n\
                        SET CUTTER OFF\r\n\
                        SET PEEL OFF\r\n\
                        SET HEAD ON\r\n\
                        SET PRINTKEY OFF\r\n\
                        SET KEY1 ON\r\n\
                        SET KEY2 ON\r\n\
                        SHIFT 0\r\n";
    [data appendData:[others dataUsingEncoding:NSASCIIStringEncoding]];
    
    [data appendData:[self cls]];
    return data;
}

+ (NSData *)selfTest {
    return [@"SELFTEST\r\n" dataUsingEncoding:NSASCIIStringEncoding];
}

+ (NSData *)cls {
    return [@"CLS\r\n" dataUsingEncoding:NSASCIIStringEncoding];
}

+ (NSData *)printSpeed:(CCCGpTsplCommandSpeed)speed {
    NSString *speedString;
    switch (speed) {
        case CCCGpTsplCommandSpeed1_5:
            speedString = @"1.5";
            break;
        case CCCGpTsplCommandSpeed2:
            speedString = @"2";
            break;
        case CCCGpTsplCommandSpeed3:
            speedString = @"3";
            break;
        default:
            speedString = @"4";
            break;
    }
    
    return [[NSString stringWithFormat:@"SPEED %@\r\n", speedString] dataUsingEncoding:NSASCIIStringEncoding];
}

+ (NSData *)referencePositionX:(NSInteger)x positionY:(NSInteger)y {
    return [[NSString stringWithFormat:@"REFERENCE %ld,%ld\r\n", (long)x, (long)y] dataUsingEncoding:NSASCIIStringEncoding];
}

+ (NSData *)offsetWithDistance:(NSInteger)offset {
    return [[NSString stringWithFormat:@"OFFSET %ld mm\r\n", (long)offset] dataUsingEncoding:NSASCIIStringEncoding];
}

+ (NSData *)printDensity:(NSInteger)density {
    density = MIN(15, MAX(0, density));
    return [[NSString stringWithFormat:@"DENSITY %ld\r\n", (long)density] dataUsingEncoding:NSASCIIStringEncoding];
}

+ (NSData *)printAreaSizeWithWidth:(NSInteger)width height:(NSInteger)height {
    return [[NSString stringWithFormat:@"SIZE %ld mm,%ld mm\r\n", (long)width, (long)height] dataUsingEncoding:NSASCIIStringEncoding];
}

+ (NSData *)gapWithDistance:(NSInteger)distance offset:(NSInteger)offset {
    return [[NSString stringWithFormat:@"GAP %ld mm,%ld mm\r\n", (long)distance, (long)offset] dataUsingEncoding:NSASCIIStringEncoding];
}

+ (NSData *)printDirection:(CCCGpTsplCommandDirection)direction mirror:(CCCGpTsplCommandMirrorType)mirror {
    return [[NSString stringWithFormat:@"DIRECTION %ld,%ld\r\n", (long)direction, (long)mirror] dataUsingEncoding:NSASCIIStringEncoding];
}

+ (NSData *)printWithSets:(NSInteger)sets copies:(NSInteger)copies {
    return [[NSString stringWithFormat:@"PRINT %ld,%ld\r\n", (long)sets, (long)copies] dataUsingEncoding:NSASCIIStringEncoding];
}

+ (CGFloat)preferredWidthFromWidth:(CGFloat)width dpi:(CGFloat)dpi {
    return [self mmFromInch:dpi] * width;
}

+ (NSData *)printImage:(UIImage *)image mode:(CCCGpTsplCommandBitmapMode)mode {
    CGImageRef cgImage = [image CGImage];
    size_t width = CGImageGetWidth(cgImage);
    size_t height = CGImageGetHeight(cgImage);
    NSInteger psize = sizeof(ARGBPixel);
    ARGBPixel * pixels = malloc(width * height * psize);
    NSMutableData* data = [[NSMutableData alloc] init];
    [self manipulateImagePixelDataWithCGImageRef:cgImage imageData:pixels];
    for (int h = 0; h < height; h++) {
        for (int w = 0; w < width; w++) {
            size_t pIndex = [self PixelIndexWithX:w y:h width:width];
            ARGBPixel pixel = pixels[pIndex];
            
            if ([self greyScaleWithPixel:pixel] <= 127) {
                u_int8_t ch = 0x00;
                [data appendBytes:&ch length:1];
            }
            else{
                u_int8_t ch = 0x01;
                [data appendBytes:&ch length:1];
            }
        }
    }
    
    const char* bytes = data.bytes;
    NSMutableData* dd = [[NSMutableData alloc] init];
    //橫向點數計算需要除以8
    NSInteger w8 = width / 8;
    //如果有餘數，點數+1
    NSInteger remain8 = width % 8;
    if (remain8 > 0) {
        w8 = w8 + 1;
    }
    
    NSString *prefix = [NSString stringWithFormat:@"BITMAP 0,0,%ld,%zu,%ld,", (long)w8, height, mode];
    [dd appendData:[prefix dataUsingEncoding:NSASCIIStringEncoding]];
    
    for (int h = 0; h < height; h ++) {
        for (int w = 0; w < w8; w ++) {
            u_int8_t n = 0;
            for (int i = 0; i < 8; i ++) {
                int x = i + w * 8;
                u_int8_t ch;
                if (x < width) {
                    size_t pindex = h * width + x;
                    ch = bytes[pindex];
                }
                else{
                    ch = 0x01;
                }
                n = n << 1;
                n = n | ch;
            }
            [dd appendBytes:&n length:1];
        }
    }
    
    [dd appendData:[@"\r\n" dataUsingEncoding:NSASCIIStringEncoding]];
    return dd;
}

+ (NSData *)testImage {
    const uint8_t cmd[] = {0x42, 0x49, 0x54, 0x4D, 0x41, 0x50, 0x20, 0x32, 0x30, 0x30, 0x2C, 0x32, 0x30, 0x30, 0x2C, 0x32, 0x2C, 0x31, 0x36, 0x2C, 0x30, 0x2C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0xFF, 0x03, 0xFF, 0x11, 0xFF, 0x18, 0xFF, 0x1C, 0x7F, 0x1E, 0x3F, 0x1F, 0x1F, 0x1F, 0x8F, 0x1F, 0xC7, 0x1F, 0xE3, 0x1F, 0xE7, 0x1F, 0xFF, 0x1F, 0xFF, 0x0D, 0x0A};
    NSUInteger cmdLength= sizeof(cmd)/sizeof(uint8_t);
    return [NSData dataWithBytes:cmd length:cmdLength];
}

+ (NSData *)getPrinterStatus {
    // <ESC>!?
    const uint8_t cmd[] = {0x27, 0x21, 0x3F, 0x0D, 0x0A};
    NSUInteger cmdLength= sizeof(cmd)/sizeof(uint8_t);
    return [NSData dataWithBytes:cmd length:cmdLength];
}

+ (NSData *)getPrinterModel {
    return [@"~!T\r\n" dataUsingEncoding:NSASCIIStringEncoding];
}

+ (NSData *)openCashDrawer {
    return [@"CASHDRAWER 0,100,100\r\n" dataUsingEncoding:NSASCIIStringEncoding];
}

+ (NSData *)feedLine:(NSInteger)line {
    return [[NSString stringWithFormat:@"FEED %ld\r\n", (long)line] dataUsingEncoding:NSASCIIStringEncoding];
}

+ (NSData *)backFeed:(NSInteger)line {
//    return [[NSString stringWithFormat:@"BACKUP %ld\r\n", (long)line] dataUsingEncoding:NSASCIIStringEncoding];
    return [[NSString stringWithFormat:@"BACKFEED %ld\r\n", (long)line] dataUsingEncoding:NSASCIIStringEncoding];
}

+ (NSData *)formFeed {
    return [@"FORMFEED\r\n" dataUsingEncoding:NSASCIIStringEncoding];
}

/// 一般开机后打印第一张卷标时，打印位置都不会准确，打印第二张标签时打印位置就会准确，若要开机 后第一张标签就要打印在正确的位置，使用该指令。
/// 注:使用该指令时，纸张高度大于或等于 30mm。
+ (NSData *)setHome {
    return [@"HOME\r\n" dataUsingEncoding:NSASCIIStringEncoding];
}

+ (NSData *)soundWith:(NSInteger)level interval:(NSInteger)interval {
    return [[NSString stringWithFormat:@"SOUND %ld,%ld\r\n", (long)level, (long)interval] dataUsingEncoding:NSASCIIStringEncoding];
}

+ (NSData *)beep {
    return [@"BEEP\r\n" dataUsingEncoding:NSASCIIStringEncoding];
}

#pragma mark -

// 参考 http://developer.apple.com/library/mac/#qa/qa1509/_index.html
+ (void)manipulateImagePixelDataWithCGImageRef:(CGImageRef)inImage imageData:(void*)oimageData {
    // Create the bitmap context
    CGContextRef cgctx = [self createARGBBitmapContextWithCGImageRef:inImage];
    if (cgctx == NULL)
    {
        // error creating context
        return;
    }
    
    // Get image width, height. We'll use the entire image.
    size_t w = CGImageGetWidth(inImage);
    size_t h = CGImageGetHeight(inImage);
    CGRect rect = {{0,0},{w,h}};
    
    // Draw the image to the bitmap context. Once we draw, the memory
    // allocated for the context for rendering will then contain the
    // raw image data in the specified color space.
    CGContextDrawImage(cgctx, rect, inImage);
    
    // Now we can get a pointer to the image data associated with the bitmap
    // context.
    void *data = CGBitmapContextGetData(cgctx);
    if (data != NULL)
    {
        CGContextRelease(cgctx);
        memcpy(oimageData, data, w * h * sizeof(u_int8_t) * 4);
        free(data);
        return;
    }
    
    // When finished, release the context
    CGContextRelease(cgctx);
    // Free image data memory for the context
    if (data)
    {
        free(data);
    }
    
    return;
}

// 参考 http://developer.apple.com/library/mac/#qa/qa1509/_index.html
+ (CGContextRef)createARGBBitmapContextWithCGImageRef:(CGImageRef)inImage {
    CGContextRef    context = NULL;
    CGColorSpaceRef colorSpace;
    void *          bitmapData;
    size_t          bitmapByteCount;
    size_t          bitmapBytesPerRow;
    
    // Get image width, height. We'll use the entire image.
    size_t pixelsWide = CGImageGetWidth(inImage);
    size_t pixelsHigh = CGImageGetHeight(inImage);
    
    // Declare the number of bytes per row. Each pixel in the bitmap in this
    // example is represented by 4 bytes; 8 bits each of red, green, blue, and
    // alpha.
    bitmapBytesPerRow   = (pixelsWide * 4);
    bitmapByteCount     = (bitmapBytesPerRow * pixelsHigh);
    
    // Use the generic RGB color space.
    colorSpace =CGColorSpaceCreateDeviceRGB();
    if (colorSpace == NULL)
    {
        return NULL;
    }
    
    // Allocate memory for image data. This is the destination in memory
    // where any drawing to the bitmap context will be rendered.
    bitmapData = malloc( bitmapByteCount );
    if (bitmapData == NULL)
    {
        CGColorSpaceRelease( colorSpace );
        return NULL;
    }
    
    // Create the bitmap context. We want pre-multiplied ARGB, 8-bits
    // per component. Regardless of what the source image format is
    // (CMYK, Grayscale, and so on) it will be converted over to the format
    // specified here by CGBitmapContextCreate.
    context = CGBitmapContextCreate (bitmapData,
                                     pixelsWide,
                                     pixelsHigh,
                                     8,      // bits per component
                                     bitmapBytesPerRow,
                                     colorSpace,
                                     kCGImageAlphaPremultipliedFirst);
    if (context == NULL)
    {
        free (bitmapData);
    }
    
    // Make sure and release colorspace before returning
    CGColorSpaceRelease( colorSpace );
    
    return context;
}

+ (u_int8_t)greyScaleWithPixel:(ARGBPixel)pixel {
    double grey = pixel.red * 0.299 + pixel.green * 0.587 + pixel.blue * 0.114;
    return MIN(255, MAX(0, (u_int8_t)grey));
}

+ (size_t)PixelIndexWithX:(size_t)x y:(size_t)y width:(size_t)width{
    return (x + (y * width));
}

+ (CGFloat)mmFromInch:(CGFloat)inch {
    return inch / 25.4;
}

@end
