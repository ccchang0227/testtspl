//
//  CCCGprinterBtOperation.h
//  TestTSPL
//
//  Created by realtouch's MBP 2019 on 2020/6/9.
//  Copyright © 2020 RealTouchApp Corp. Ltd. All rights reserved.
//

#import <CoreBluetooth/CoreBluetooth.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, CCCGprinterBtOperationType) {
    CCCGprinterBtOperationTypeUnknown,
    CCCGprinterBtOperationTypeWrite,
    CCCGprinterBtOperationTypeNotify,
    CCCGprinterBtOperationTypeRead
};

@interface CCCGprinterBtOperation : NSOperation

- (instancetype)initWithType:(CCCGprinterBtOperationType)type
                  peripheral:(nullable CBPeripheral *)peripheral
              characteristic:(nullable CBCharacteristic *)characteristic;
- (instancetype)initWithType:(CCCGprinterBtOperationType)type
                  peripheral:(nullable CBPeripheral *)peripheral
              characteristic:(nullable CBCharacteristic *)characteristic
                       value:(nullable id)value
                   writeType:(CBCharacteristicWriteType)writeType NS_DESIGNATED_INITIALIZER;

@property (readonly, nonatomic) CCCGprinterBtOperationType type;
@property (readonly, strong, nonatomic) id _Nullable value;

// Default is 5 seconds.
@property (nonatomic) NSTimeInterval timeoutInterval;

@property (readonly, strong, nonatomic) id _Nullable response;
@property (readonly, nonatomic) BOOL isExecuteSuccess;
@property (readonly, nonatomic) BOOL isTimeout;

- (void)complete;
- (void)completeWithResponse:(nullable id)response;

@end

NS_ASSUME_NONNULL_END
