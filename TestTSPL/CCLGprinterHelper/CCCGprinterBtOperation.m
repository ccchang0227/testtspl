//
//  CCCGprinterBtOperation.m
//  TestTSPL
//
//  Created by realtouch's MBP 2019 on 2020/6/9.
//  Copyright © 2020 RealTouchApp Corp. Ltd. All rights reserved.
//

#import "CCCGprinterBtOperation.h"

@interface CCCGprinterBtOperation ()

@property (weak, nonatomic) CBPeripheral * _Nullable peripheral;
@property (weak, nonatomic) CBCharacteristic * _Nullable characteristic;
@property (nonatomic) CBCharacteristicWriteType writeType;

@property (strong, nonatomic) dispatch_semaphore_t signal;

@end

@implementation CCCGprinterBtOperation

- (instancetype)init {
    return [self initWithType:CCCGprinterBtOperationTypeUnknown
                   peripheral:nil
               characteristic:nil
                        value:nil
                    writeType:CBCharacteristicWriteWithoutResponse];
}

- (instancetype)initWithType:(CCCGprinterBtOperationType)type
                  peripheral:(nullable CBPeripheral *)peripheral
              characteristic:(nullable CBCharacteristic *)characteristic {
    return [self initWithType:type
                   peripheral:peripheral
               characteristic:characteristic
                        value:nil
                    writeType:CBCharacteristicWriteWithoutResponse];
}

- (instancetype)initWithType:(CCCGprinterBtOperationType)type
                  peripheral:(nullable CBPeripheral *)peripheral
              characteristic:(nullable CBCharacteristic *)characteristic
                       value:(nullable id)value
                   writeType:(CBCharacteristicWriteType)writeType {
    self = [super init];
    if (self) {
        _type = type;
        self.peripheral = peripheral;
        self.characteristic = characteristic;
        _value = value;
        self.writeType = writeType;
        
        _timeoutInterval = 5.0;
        
        _isExecuteSuccess = NO;
        _isTimeout = NO;
        
        self.signal = dispatch_semaphore_create(0);
    }
    return self;
}

- (void)setTimeoutInterval:(NSTimeInterval)timeoutInterval {
    if (self.isExecuteSuccess) {
        return;
    }
    
    _timeoutInterval = timeoutInterval;
}

- (void)start {
    if (self.isCancelled) {
        return;
    }
    
    [super start];
}

- (void)main {
    if (self.isCancelled) {
        return;
    }
    
    [NSThread sleepForTimeInterval:0.01];
    
    if (self.isCancelled) {
        return;
    }
    
    if (!self.peripheral || !self.characteristic) {
        return;
    }
    
    if (self.isCancelled) {
        return;
    }
    
    switch (self.type) {
        case CCCGprinterBtOperationTypeRead:
            if ((self.characteristic.properties & CBCharacteristicPropertyRead) == 0) {
                return;
            }
            
            [self.peripheral readValueForCharacteristic:self.characteristic];
            
            break;
        case CCCGprinterBtOperationTypeWrite:
            if (!self.value || ![self.value isKindOfClass:[NSData class]]) {
                return;
            }
            if ((self.characteristic.properties & CBCharacteristicPropertyWrite) == 0 &&
                (self.characteristic.properties & CBCharacteristicPropertyWriteWithoutResponse) == 0) {
                return;
            }
            
            [self.peripheral writeValue:(NSData *)self.value
                      forCharacteristic:self.characteristic
                                   type:self.writeType];
            
            break;
        case CCCGprinterBtOperationTypeNotify:
            if (!self.value || ![self.value isKindOfClass:[NSNumber class]]) {
                return;
            }
            if ((self.characteristic.properties & CBCharacteristicPropertyNotify) == 0 &&
                (self.characteristic.properties & CBCharacteristicPropertyIndicate) == 0) {
                return;
            }
            
            BOOL notifyValue = [self.value boolValue];
            [self.peripheral setNotifyValue:notifyValue forCharacteristic:self.characteristic];
            
            break;
        default:
            return;
    }
    
    if (self.isCancelled) {
        return;
    }
    
    _isExecuteSuccess = YES;
    
    // timeout n seconds
    long result = dispatch_semaphore_wait(self.signal, dispatch_time(DISPATCH_TIME_NOW, (int64_t)(self.timeoutInterval * NSEC_PER_SEC)));
    
    if (self.isCancelled) {
        return;
    }
    
    if (result != 0) {
        if (!self.response) {
            _isTimeout = YES;
            
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"Time out", nil),
                                       NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Time out", nil)};
            _response = [NSError errorWithDomain:@"BtSerialOperationError" code:-8763 userInfo:userInfo];
        }
    }
    
}

- (void)cancel {
    [super cancel];
    
    dispatch_semaphore_signal(self.signal);
    
}

- (void)complete {
    [self completeWithResponse:nil];
}

- (void)completeWithResponse:(nullable id)response {
    _response = response;
    
    dispatch_semaphore_signal(self.signal);
}

@end
