//
//  CCLGprinterCommand.h
//  CCLGprinter
//
//  Created by realtouch on 2017/3/7.
//  Copyright © 2017年 realtouch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, CCLGprinterCommandType) {
    CCLGprinterCommandTypeGetPrinterName = 1,
    CCLGprinterCommandTypeRealTimeStatus,
    CCLGprinterCommandTypePrintImage
};

@interface CCLGprinterCommand : NSObject

/** 
 初始化印表機(CCLGprinterHelper以外請勿調用)
 
 @warning 在CCLGprinterHelper以外自己產生指令時不需手動自己調用，CCLGprinterHelper內會自動加上
 */
+ (NSData *)commandInitializePrinter __attribute__((deprecated("CCLGprinterHelper以外請勿調用")));


/**
 列印並且往前走紙n行
 
 @param n 行數(0≤n≤255)
 */
+ (NSData *)commandPrintAndFeedLines:(uint8_t)n;


/** 印表機直接切紙 */
+ (NSData *)commandCutPaper;

/** 打開錢箱 */
+ (NSData *)commandOpenCrashBox;

/** 蜂鳴器 */
+ (NSData *)commandVoice;

/**
 印表機即時狀態(CCLGprinterHelper以外請勿調用)
 
 @warning 在CCLGprinterHelper以外自己產生指令時不需手動自己調用，CCLGprinterHelper內會自動加上
 */
+ (NSData *)commandPrinterRealTimeStatus __attribute__((deprecated("CCLGprinterHelper以外請勿調用")));


/** 列印圖片 */
+ (NSData *)commandPrintImage:(UIImage *)image;


/** 列印自我測試資訊 */
+ (NSData *)commandPrintSelfTest;


- (instancetype)init NS_UNAVAILABLE;

@end
