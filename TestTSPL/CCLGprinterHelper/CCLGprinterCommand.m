//
//  CCLGprinterCommand.m
//  CCLGprinter
//
//  Created by realtouch on 2017/3/7.
//  Copyright © 2017年 realtouch. All rights reserved.
//

#import "CCLGprinterCommand.h"

typedef struct {
    /// Alpha component
    UInt8 alpha;
    /// Red component
    UInt8 red;
    /// Green component
    UInt8 green;
    /// Blue component
    UInt8 blue;
} ARGBPixel;

@interface CCLGprinterCommand ()


@end

@implementation CCLGprinterCommand

+ (NSData *)commandInitializePrinter {
    const uint8_t cmd[] = {0x1b,0x40};
    NSUInteger cmdLength= sizeof(cmd)/sizeof(uint8_t);
    return [NSMutableData dataWithBytes:cmd length:cmdLength];
}

+ (NSData *)commandPrinterRealTimeStatus {
    const uint8_t cmd[] = {0x10,0x04,0x01,0x10,0x04,0x02,0x10,0x04,0x03,0x10,0x04,0x04};
    NSUInteger cmdLength= sizeof(cmd)/sizeof(uint8_t);
    return [NSMutableData dataWithBytes:cmd length:cmdLength];
}

+ (NSData *)commandPrintAndFeedLines:(uint8_t)n {
    const uint8_t cmd[] = {0x1b,0x64,n};
    NSUInteger cmdLength= sizeof(cmd)/sizeof(uint8_t);
    return [NSMutableData dataWithBytes:cmd length:cmdLength];
}

+ (NSData *)commandCutPaper {
    const uint8_t cmd[] = {0x1d,0x56,0x00};
    NSUInteger cmdLength= sizeof(cmd)/sizeof(uint8_t);
    return [NSMutableData dataWithBytes:cmd length:cmdLength];
}

+ (NSData *)commandOpenCrashBox {
    const uint8_t cmd[] = {0x1b,0x70,0x00,0x64,0x64};
//    const uint8_t cmd[] = {0x1b,0x70,0x00,0x64,0xfa};
    NSUInteger cmdLength= sizeof(cmd)/sizeof(uint8_t);
    return [NSMutableData dataWithBytes:cmd length:cmdLength];
}

+ (NSData *)commandVoice {
//    const uint8_t cmd[] = {0x1b,0x42,0x01,0x02};
    const uint8_t cmd[] = {0x1b,0x43,0x03,0x04,0x03};
//    const uint8_t cmd[] = {0x1b,0x70,0x00,0x64,0xfa};
    NSUInteger cmdLength= sizeof(cmd)/sizeof(uint8_t);
    return [NSMutableData dataWithBytes:cmd length:cmdLength];
}


+ (NSData *)commandPrintSelfTest {
    const uint8_t cmd[] = {0x1f,0x1b,0x1f,0x93,0x10,0x11,0x12,0x15,0x16,0x17,0x10,0x00};
    NSUInteger cmdLength= sizeof(cmd)/sizeof(uint8_t);
    return [NSMutableData dataWithBytes:cmd length:cmdLength];
}

+ (NSData *)commandPrintImage:(UIImage *)image {
    CGImageRef cgImage = [image CGImage];
    size_t width = CGImageGetWidth(cgImage);
    size_t height = CGImageGetHeight(cgImage);
    NSInteger psize = sizeof(ARGBPixel);
    ARGBPixel * pixels = malloc(width * height * psize);
    NSMutableData* data = [[NSMutableData alloc] init];
    [self manipulateImagePixelDataWithCGImageRef:cgImage imageData:pixels];
    for (int h = 0; h < height; h++) {
        for (int w = 0; w < width; w++) {
            size_t pIndex = [self PixelIndexWithX:w y:h width:width];
            ARGBPixel pixel = pixels[pIndex];
            
            if ([self PixelBrightnessWithRed:pixel.red green:pixel.green blue:pixel.blue] <= 127) {
                u_int8_t ch = 0x01;
                [data appendBytes:&ch length:1];
            }
            else{
                u_int8_t ch = 0x00;
                [data appendBytes:&ch length:1];
            }
        }
    }
    
    
    
    
    const char* bytes = data.bytes;
    NSMutableData* dd = [[NSMutableData alloc] init];
    //橫向點數計算需要除以8
    NSInteger w8 = width / 8;
    //如果有餘數，點數+1
    NSInteger remain8 = width % 8;
    if (remain8 > 0) {
        w8 = w8 + 1;
    }
    /**
     根據公式計算出打印指令需要的參數
     指令：十六進制碼1D 76 30 m xL xH yL yH d1 ... dk
     m為模式，如果是58毫秒打印機，m = 1即可
     xL為寬度/ 256的餘數，由於橫向點數計算為像素數/ 8，因此需要xL = width /（8 * 256）
     xH為寬/ 256的整數
     yL為高度/ 256的餘數
     yH為高度/ 256的整數
     **/
    NSInteger xL = w8 % 256;
    NSInteger xH = width / (88 * 256);
    NSInteger yL = height % 256;
    NSInteger yH = height / 256;
    
    const char cmd[] = {0x1d,0x76,0x30,0,xL,xH,yL,yH};
    [dd appendBytes:cmd length:8];
    
    for (int h = 0; h < height; h++) {
        for (int w = 0; w < w8; w++) {
            u_int8_t n = 0;
            for (int i=0; i<8; i++) {
                int x = i + w * 8;
                u_int8_t ch;
                if (x < width) {
                    size_t pindex = h * width + x;
                    ch = bytes[pindex];
                }
                else{
                    ch = 0x00;
                }
                n = n << 1;
                n = n | ch;
            }
            [dd appendBytes:&n length:1];
        }
    }
    return dd;
}

// 参考 http://developer.apple.com/library/mac/#qa/qa1509/_index.html
+ (void)manipulateImagePixelDataWithCGImageRef:(CGImageRef)inImage imageData:(void*)oimageData {
    // Create the bitmap context
    CGContextRef cgctx = [self createARGBBitmapContextWithCGImageRef:inImage];
    if (cgctx == NULL)
    {
        // error creating context
        return;
    }
    
    // Get image width, height. We'll use the entire image.
    size_t w = CGImageGetWidth(inImage);
    size_t h = CGImageGetHeight(inImage);
    CGRect rect = {{0,0},{w,h}};
    
    // Draw the image to the bitmap context. Once we draw, the memory
    // allocated for the context for rendering will then contain the
    // raw image data in the specified color space.
    CGContextDrawImage(cgctx, rect, inImage);
    
    // Now we can get a pointer to the image data associated with the bitmap
    // context.
    void *data = CGBitmapContextGetData(cgctx);
    if (data != NULL)
    {
        CGContextRelease(cgctx);
        memcpy(oimageData, data, w * h * sizeof(u_int8_t) * 4);
        free(data);
        return;
    }
    
    // When finished, release the context
    CGContextRelease(cgctx);
    // Free image data memory for the context
    if (data)
    {
        free(data);
    }
    
    return;
}

// 参考 http://developer.apple.com/library/mac/#qa/qa1509/_index.html
+ (CGContextRef)createARGBBitmapContextWithCGImageRef:(CGImageRef)inImage {
    CGContextRef    context = NULL;
    CGColorSpaceRef colorSpace;
    void *          bitmapData;
    size_t          bitmapByteCount;
    size_t          bitmapBytesPerRow;
    
    // Get image width, height. We'll use the entire image.
    size_t pixelsWide = CGImageGetWidth(inImage);
    size_t pixelsHigh = CGImageGetHeight(inImage);
    
    // Declare the number of bytes per row. Each pixel in the bitmap in this
    // example is represented by 4 bytes; 8 bits each of red, green, blue, and
    // alpha.
    bitmapBytesPerRow   = (pixelsWide * 4);
    bitmapByteCount     = (bitmapBytesPerRow * pixelsHigh);
    
    // Use the generic RGB color space.
    colorSpace =CGColorSpaceCreateDeviceRGB();
    if (colorSpace == NULL)
    {
        return NULL;
    }
    
    // Allocate memory for image data. This is the destination in memory
    // where any drawing to the bitmap context will be rendered.
    bitmapData = malloc( bitmapByteCount );
    if (bitmapData == NULL)
    {
        CGColorSpaceRelease( colorSpace );
        return NULL;
    }
    
    // Create the bitmap context. We want pre-multiplied ARGB, 8-bits
    // per component. Regardless of what the source image format is
    // (CMYK, Grayscale, and so on) it will be converted over to the format
    // specified here by CGBitmapContextCreate.
    context = CGBitmapContextCreate (bitmapData,
                                     pixelsWide,
                                     pixelsHigh,
                                     8,      // bits per component
                                     bitmapBytesPerRow,
                                     colorSpace,
                                     kCGImageAlphaPremultipliedFirst);
    if (context == NULL)
    {
        free (bitmapData);
    }
    
    // Make sure and release colorspace before returning
    CGColorSpaceRelease( colorSpace );
    
    return context;
}

+ (u_int8_t)PixelBrightnessWithRed:(u_int8_t)red green:(u_int8_t)green blue:(u_int8_t)blue {
    int level = ((int)red + (int)green + (int)blue)/3;
    return level;
}

+ (size_t)PixelIndexWithX:(size_t)x y:(size_t)y width:(size_t)width{
    return (x + (y * width));
}


@end
