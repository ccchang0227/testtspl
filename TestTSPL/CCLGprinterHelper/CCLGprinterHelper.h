//
//  CCLGprinterHelper.h
//  CCLGprinter
//
//  Created by realtouch on 2017/3/8.
//  Copyright © 2017年 realtouch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CCLGprinterImageHelper.h"
#import "CCLGprinterCommand.h"
#import "CCCBluetoothGprinter.h"

typedef NS_ENUM(NSInteger, CCLGprinterManagerError) {
    CCLGprinterManagerUnknownError = 1,
    CCLGprinterManagerNetWorkError,
    CCLGprinterManagerBadParamError,
    CCLGprinterManagerResponseDataError,
    CCLGprinterManagerPrinterStatusError
};


FOUNDATION_EXPORT NSString *const CCLGprinterHelperErrorDomain;

typedef void (^CCLGprinterHelperCompletionBlock)(BOOL success, NSString *printerStatus, NSError *error);
typedef void (^CCLGprinterHelperFailureBlock)(NSError *error);

typedef void (^CCLGprinterHelperScanBtPrinterBlock)(CCCBluetoothGprinter *printer, BOOL isTimeout);

#pragma mark - CCLGprinterHelper

/**
 Gprinter SDK
 @class CCLGprinterHelper
 @author C.C.Lin
 @version 1.0
 @date 2017/03/08
 @see Gprinter Program Manual
 */
@interface CCLGprinterHelper : NSObject



#pragma mark - Property

#pragma mark - Method

//#pragma mark Init
//+ (CCLGprinterHelper *)sharedInstance;
//- (instancetype)init NS_UNAVAILABLE;



#pragma mark 傳送指令

/** 傳送列印圖片 */
- (void)sendDataToHost:(NSString *)host
                  port:(uint16_t)port
                 image:(UIImage *)image
     completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler
        failureHandler:(CCLGprinterHelperFailureBlock)failureHandler;
/** 傳送列印圖片Array */
- (void)sendDataToHost:(NSString *)host
                  port:(uint16_t)port
                images:(NSArray *)images
     completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler
        failureHandler:(CCLGprinterHelperFailureBlock)failureHandler ;

- (void)sendBigDataToHost:(NSString *)host port:(uint16_t)port images:(NSArray *)images completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler failureHandler:(CCLGprinterHelperFailureBlock)failureHandler;

/** 傳送列印Data */
- (void)sendDataToHost:(NSString *)host
                  port:(uint16_t)port
             writeData:(NSMutableData *)writeData
     completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler failureHandler:(CCLGprinterHelperFailureBlock)failureHandler;
/** 傳送列印Data 但不裁切*/
- (void)sendDataWithNoCutToHost:(NSString *)host
                           port:(uint16_t)port
                         images:(NSArray *)images
              completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler
                 failureHandler:(CCLGprinterHelperFailureBlock)failureHandler;

/** 傳送指令 */
- (void)sendDataToHost:(NSString *)host
                  port:(uint16_t)port
                  data:(NSData *)data
     completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler
        failureHandler:(CCLGprinterHelperFailureBlock)failureHandler;


/** 傳送列印測試頁 */
- (void)printTestPageToHost:(NSString *)host
                       port:(uint16_t)port
          completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler
             failureHandler:(CCLGprinterHelperFailureBlock)failureHandler;

/** 開錢箱 */
- (void)openCarshBoxToHost:(NSString *)host
                      port:(uint16_t)port
         completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler
            failureHandler:(CCLGprinterHelperFailureBlock)failureHandler;


/** 蜂鳴器 */ ///H.T.Lin

- (void)makeVoiceToHost:(NSString *)host
                   port:(uint16_t)port
      completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler
         failureHandler:(CCLGprinterHelperFailureBlock)failureHandler;

#pragma mark - C.C.Chang (2020.06.09 updated)

@property (readonly, retain, nonatomic) CBCentralManager *centerManager;
@property (readonly, retain, nonatomic) NSMutableArray<CCCBluetoothGprinter *> *discoveredPeripherals;
@property (readonly, retain, nonatomic) NSMutableArray<CCCBluetoothGprinter *> *connectedPeripherals;

@property (readonly, nonatomic, getter=isBluetoothOn) BOOL bluetoothOn;

/// 開始掃描
- (void)scanPrintersWithHandler:(CCLGprinterHelperScanBtPrinterBlock)handler;
/// 停止掃描
- (void)stopScan;

/// 連線
- (void)connectToPrinter:(CCCBluetoothGprinter *)printer
                 timeout:(NSTimeInterval)timeoutInterval
              completion:(void(^)(NSError *))completionHandler;
/// 中斷連線
- (void)disconnectPrinter:(CCCBluetoothGprinter *)printer
               completion:(void(^)(NSError *))completionHandler;

/// 從UUID取得/產生CCCBluetoothGprinter物件
- (CCCBluetoothGprinter *)bluetoothPrinterWithUUID:(NSUUID *)uuid;

/// 設定標籤紙大小 (單位mm)
- (void)setPrintLabelSize:(CGSize)labelSize;

/**
 Notice!!!!
 傳送列印指令前，若未連線，會先進行連線，
 指令傳送後，"不會"中斷連線，會保持連線狀態
 */

/// 傳送列印Data
- (void)sendDataToBluetoothPrinter:(CCCBluetoothGprinter *)printer
                              data:(NSData *)data
                 completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler
                    failureHandler:(CCLGprinterHelperFailureBlock)failureHandler;
/// 傳送列印圖片
- (void)sendDataToBluetoothPrinter:(CCCBluetoothGprinter *)printer
                             image:(UIImage *)image
                 completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler
                    failureHandler:(CCLGprinterHelperFailureBlock)failureHandler;

/// 傳送多張列印圖片 (未測試)
- (void)sendDataToBluetoothPrinter:(CCCBluetoothGprinter *)printer
                            images:(NSArray<UIImage *> *)images
                 completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler
                    failureHandler:(CCLGprinterHelperFailureBlock)failureHandler;

@end
