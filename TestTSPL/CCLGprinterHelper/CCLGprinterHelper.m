//
//  CCLGprinterHelper.m
//  CCLGprinter
//
//  Created by realtouch on 2017/3/8.
//  Copyright © 2017年 realtouch. All rights reserved.
//

#import "CCLGprinterHelper.h"
#import "CocoaAsyncSocket.h"

static const NSTimeInterval kDefaultSocketTimeout = 15;

NSString *const CCLGprinterHelperErrorDomain = @"CCLGprinterHelperErrorDomain";

@interface CCLGprinterHelper () <GCDAsyncSocketDelegate, CBCentralManagerDelegate, CBPeripheralDelegate> {
    dispatch_source_t timeoutTimer;
}

@property (retain, nonatomic) GCDAsyncSocket *socket;
@property (copy, nonatomic) NSData *writeData;
@property (copy, nonatomic) CCLGprinterHelperCompletionBlock completionHandler;
@property (copy, nonatomic) CCLGprinterHelperFailureBlock failureHandler;

@property (assign, nonatomic) BOOL isKeepScan;
@property (retain, nonatomic) CBCentralManager *centerManager;
@property (retain, nonatomic) NSMutableArray *discoveredPeripherals;
@property (retain, nonatomic) NSMutableArray *connectedPeripherals;

@property (retain, nonatomic) NSMutableDictionary<NSUUID *, NSError *> *customErrors;

@property (copy, nonatomic) CCLGprinterHelperScanBtPrinterBlock scanPrinterHandler;

@property (nonatomic) CGSize labelSize;

@end

@implementation CCLGprinterHelper

+ (CCLGprinterHelper *)sharedInstance {
    static dispatch_once_t pred;
    static CCLGprinterHelper *shared = nil;
    
    dispatch_once(&pred, ^{
        shared = [[CCLGprinterHelper alloc] initPrinter];
    });
    return shared;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (instancetype)initPrinter {
    self = [super init];
    if (self) {
        _bluetoothOn = NO;
        
        NSDictionary *option = @{ CBCentralManagerOptionShowPowerAlertKey : @YES };
        self.centerManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:option];
        self.discoveredPeripherals = [[NSMutableArray alloc] init];
        self.connectedPeripherals = [[NSMutableArray alloc] init];
        
        self.customErrors = [[NSMutableDictionary alloc] init];
        
        self.labelSize = CGSizeMake(40, 30);
    }
    return self;
}

- (void)dealloc {
    if(timeoutTimer){
        dispatch_source_cancel(timeoutTimer);
        dispatch_release(timeoutTimer);
    }
    
    [_socket release];
    [_writeData release];
    [_completionHandler release];
    [_failureHandler release];
    [_scanPrinterHandler release];
    [_customErrors release];
    [_centerManager release];
    [_discoveredPeripherals release];
    [_connectedPeripherals release];
    [super dealloc];
}


#pragma mark - Public


- (void)sendDataToHost:(NSString *)host port:(uint16_t)port data:(NSData *)data completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler failureHandler:(CCLGprinterHelperFailureBlock)failureHandler {
    
    if(![CCLGprinterHelper isStringNotEmpty:host]){
        NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Invalid host parameter (nil or \"\"). Should be a domain name or IP address string.", nil) code:CCLGprinterManagerBadParamError];
        failureHandler(errorMessage);
        return;
    }
    
    if([data length] == 0){
        NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Invalid image", nil) code:CCLGprinterManagerBadParamError];
        failureHandler(errorMessage);
        return;
    }
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    
    
    NSMutableData *writeData = [[[NSMutableData alloc] init] autorelease];
    [writeData appendData:[CCLGprinterCommand commandInitializePrinter]];
    [writeData appendData:data];
    [writeData appendData:[CCLGprinterCommand commandPrinterRealTimeStatus]];
    
    
#pragma clang diagnostic pop
    

    NSLog(@"Connecting to \"%@\" on port %hu...", host, port);
    NSError *error = nil;
    if([self.socket connectToHost:host onPort:port withTimeout:kDefaultSocketTimeout error:&error]){
        self.writeData = writeData;
        self.completionHandler = completionHandler;
        self.failureHandler = failureHandler;
    }
    else{
        NSLog(@"Networking - Error connecting: %@", error);
        failureHandler(error);
        return;
    }
}



- (void)sendDataToHost:(NSString *)host port:(uint16_t)port image:(UIImage *)image completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler failureHandler:(CCLGprinterHelperFailureBlock)failureHandler {
    
    if(!image){
        NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Invalid image", nil) code:CCLGprinterManagerBadParamError];
        failureHandler(errorMessage);
        return;
    }
    
    NSMutableData *writeData = [[[NSMutableData alloc] init] autorelease];
    [writeData appendData:[CCLGprinterCommand commandPrintImage:image]];
    [writeData appendData:[CCLGprinterCommand commandPrintAndFeedLines:4]];
    [writeData appendData:[CCLGprinterCommand commandCutPaper]];

    [self sendDataToHost:host port:port data:writeData completionHandler:completionHandler failureHandler:failureHandler];
}

- (void)sendDataToHost:(NSString *)host port:(uint16_t)port images:(NSArray *)images completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler failureHandler:(CCLGprinterHelperFailureBlock)failureHandler {
    
    if(!images){
        NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Invalid image", nil) code:CCLGprinterManagerBadParamError];
        failureHandler(errorMessage);
        return;
    }
    
    NSMutableData *writeData = [[[NSMutableData alloc] init] autorelease];
    
    for (UIImage *image in images) {
        
//        if (0) {
            
        if (image.size.height > 1000) {
            //設定原始影像
            UIImage *originalImage =image;
            float y = 0.0f;
            float lessHeight = originalImage.size.height;

            while (y < originalImage.size.height) {
                //設定剪裁影像的位置與大小
                CGImageRef imageRef;
                if (lessHeight > 1000) {
                    imageRef = CGImageCreateWithImageInRect([originalImage CGImage], CGRectMake(0, y, originalImage.size.width ,1000));
                }else {
                    imageRef = CGImageCreateWithImageInRect([originalImage CGImage], CGRectMake(0, y, originalImage.size.width ,lessHeight));
                }


                //剪裁影像
                UIImage *copiedImage = [UIImage imageWithCGImage:imageRef];
                [writeData appendData:[CCLGprinterCommand commandPrintImage:copiedImage]];
                y = y+ 1000;
                lessHeight = lessHeight - 1000;
            }
            
            [writeData appendData:[CCLGprinterCommand commandPrintAndFeedLines:4]];
            [writeData appendData:[CCLGprinterCommand commandCutPaper]];
        }else {

            [writeData appendData:[CCLGprinterCommand commandPrintImage:image]];
            [writeData appendData:[CCLGprinterCommand commandPrintAndFeedLines:4]];
            [writeData appendData:[CCLGprinterCommand commandCutPaper]];
        }
        
    }
    
    [self sendDataToHost:host port:port data:writeData completionHandler:completionHandler failureHandler:failureHandler];
    
    
}



- (void)sendDataWithNoCutToHost:(NSString *)host port:(uint16_t)port images:(NSArray *)images completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler failureHandler:(CCLGprinterHelperFailureBlock)failureHandler {
    
    if(!images){
        NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Invalid image", nil) code:CCLGprinterManagerBadParamError];
        failureHandler(errorMessage);
        return;
    }
    
    NSMutableData *writeData = [[[NSMutableData alloc] init] autorelease];
    
    for (UIImage *image in images) {
        
//        if (0) {
            
        if (image.size.height > 1000) {
            //設定原始影像
            UIImage *originalImage =image;
            float y = 0.0f;
            float lessHeight = originalImage.size.height;

            while (y < originalImage.size.height) {
                //設定剪裁影像的位置與大小
                CGImageRef imageRef;
                if (lessHeight > 1000) {
                    imageRef = CGImageCreateWithImageInRect([originalImage CGImage], CGRectMake(0, y, originalImage.size.width ,1000));
                }else {
                    imageRef = CGImageCreateWithImageInRect([originalImage CGImage], CGRectMake(0, y, originalImage.size.width ,lessHeight));
                }


                //剪裁影像
                UIImage *copiedImage = [UIImage imageWithCGImage:imageRef];
                [writeData appendData:[CCLGprinterCommand commandPrintImage:copiedImage]];
                y = y+ 1000;
                lessHeight = lessHeight - 1000;
            }
            
//            [writeData appendData:[CCLGprinterCommand commandPrintAndFeedLines:4]];
//            [writeData appendData:[CCLGprinterCommand commandCutPaper]];
        }else {

            [writeData appendData:[CCLGprinterCommand commandPrintImage:image]];
//            [writeData appendData:[CCLGprinterCommand commandPrintAndFeedLines:4]];
//            [writeData appendData:[CCLGprinterCommand commandCutPaper]];
        }
        
    }
    
    [self sendDataToHost:host port:port data:writeData completionHandler:completionHandler failureHandler:failureHandler];
    
    
}




- (void)sendBigDataToHost:(NSString *)host port:(uint16_t)port images:(NSArray *)images completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler failureHandler:(CCLGprinterHelperFailureBlock)failureHandler {
    
    if(!images){
        NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Invalid image", nil) code:CCLGprinterManagerBadParamError];
        failureHandler(errorMessage);
        return;
    }
    
    NSMutableData *writeData = [[[NSMutableData alloc] init] autorelease];
    
    
    
    for (UIImage *image in images) {
        
//        if (0) {
            
        if (image.size.height > 1000) {
            //設定原始影像
            UIImage *originalImage =image;
            float y = 0.0f;
            float lessHeight = originalImage.size.height;

            while (y < originalImage.size.height) {
                //設定剪裁影像的位置與大小
                CGImageRef imageRef;
                if (lessHeight > 1000) {
                    imageRef = CGImageCreateWithImageInRect([originalImage CGImage], CGRectMake(0, y, originalImage.size.width ,1000));
                }else {
                    imageRef = CGImageCreateWithImageInRect([originalImage CGImage], CGRectMake(0, y, originalImage.size.width ,lessHeight));
                }


                //剪裁影像
                UIImage *copiedImage = [UIImage imageWithCGImage:imageRef];
                
                y = y+ 1000;
                lessHeight = lessHeight - 1000;
                [writeData appendData:[CCLGprinterCommand commandPrintImage:copiedImage]];
                [self sendDataToHost:host port:port data:writeData completionHandler:completionHandler failureHandler:failureHandler];
            }
            
            [writeData appendData:[CCLGprinterCommand commandPrintAndFeedLines:4]];
            [writeData appendData:[CCLGprinterCommand commandCutPaper]];
            [self sendDataToHost:host port:port data:writeData completionHandler:completionHandler failureHandler:failureHandler];
        }else {

            [writeData appendData:[CCLGprinterCommand commandPrintImage:image]];
            [writeData appendData:[CCLGprinterCommand commandPrintAndFeedLines:4]];
            [writeData appendData:[CCLGprinterCommand commandCutPaper]];
            [self sendDataToHost:host port:port data:writeData completionHandler:completionHandler failureHandler:failureHandler];
        }
        
    }
    
    
    
    
    
//    [self sendDataToHost:host port:port data:writeData completionHandler:completionHandler failureHandler:failureHandler];
}


- (void)sendDataToHost:(NSString *)host port:(uint16_t)port writeData:(NSMutableData *)writeData completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler failureHandler:(CCLGprinterHelperFailureBlock)failureHandler {
//    
//    if(!image){
//        NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Invalid image", nil) code:CCLGprinterManagerBadParamError];
//        failureHandler(errorMessage);
//        return;
//    }
//    
//    NSMutableData *writeData = [[[NSMutableData alloc] init] autorelease];
//    [writeData appendData:[CCLGprinterCommand commandPrintImage:image]];
//    [writeData appendData:[CCLGprinterCommand commandPrintAndFeedLines:4]];
//    [writeData appendData:[CCLGprinterCommand commandCutPaper]];
    
    [self sendDataToHost:host port:port data:writeData completionHandler:completionHandler failureHandler:failureHandler];
}



- (void)printTestPageToHost:(NSString *)host port:(uint16_t)port completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler failureHandler:(CCLGprinterHelperFailureBlock)failureHandler {
    
    NSMutableData *writeData = [[[NSMutableData alloc] init] autorelease];
    [writeData appendData:[CCLGprinterCommand commandPrintSelfTest]];
    
    [self sendDataToHost:host port:port data:writeData completionHandler:completionHandler failureHandler:failureHandler];
}




#pragma mark - GCDAsyncSocketDelegate

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port {
    NSLog(@"socket:%p didConnectToHost:%@ port:%hu", sock, host, port);
    [self reloadRealTimeStatusWithSocket:sock];
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(nullable NSError *)err {
    NSLog(@"socketDidDisconnect:%p withError: %@", sock, err);
    if(self.failureHandler){
        self.failureHandler(err);
        return;
    }
    else{
        self.completionHandler = nil;
        self.failureHandler = nil;
    }
}


- (void)socket:(GCDAsyncSocket*)sock didWriteDataWithTag:(long)tag {
    NSLog(@"socket:%p didWriteDataWithTag:%ld", sock, tag);
    [sock readDataWithTimeout:kDefaultSocketTimeout tag:tag];
}


- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag {
    NSLog(@"socket:%p didReadData:withTag:%ld", sock, tag);
    
    switch (tag) {
        case CCLGprinterCommandTypeRealTimeStatus:{
            [self checkPrinterStatusWithResponseData:data];
            break;
        }
        case CCLGprinterCommandTypePrintImage:{
            [self checkPrintResultWithResponseData:data];
            break;
        }
        default:{
            NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Unknown Error", nil) code:CCLGprinterManagerUnknownError];
            self.failureHandler(errorMessage);
            return;
        }
    }
}


//- (void)socket:(GCDAsyncSocket *)sock didReadPartialDataOfLength:(NSUInteger)partialLength tag:(long)tag
//{
//
//    NSLog(@"didReadPartialData,length:%ld,tag:%ld",partialLength,tag);
//
//}


#pragma mark - 資料處理

- (void)checkPrinterStatusWithResponseData:(NSData *)data {
    if([data length] != 4){
        NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Response Data Error", nil) code:CCLGprinterManagerResponseDataError];
        self.failureHandler(errorMessage);
        return;
    }
    
    NSString *statusBitsString = [self printerStatusFromData:data];
    
    NSLog(@"checkPrinterStatusWithResponseData = %@", statusBitsString);
    
    const uint8_t *bytes = [data bytes];
    
    uint8_t byte = bytes[0];
    if((byte & 0b00001000) == 0){
        [self.socket writeData:self.writeData withTimeout:-1 tag:CCLGprinterCommandTypePrintImage];
    }
    else{
        NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Printer Status Error", nil) code:CCLGprinterManagerPrinterStatusError];
        self.failureHandler(errorMessage);
        return;
    }
}

- (void)checkPrintResultWithResponseData:(NSData *)data {
    if([data length] != 4){
        NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Response Data Error", nil) code:CCLGprinterManagerResponseDataError];
        self.failureHandler(errorMessage);
        return;
    }
    
    NSString *statusBitsString = [self printerStatusFromData:data];
    
    NSLog(@"checkPrintResultWithResponseData = %@", statusBitsString);
    
    const uint8_t *bytes = [data bytes];
    
    uint8_t byte = bytes[0];
    if((byte & 0b00001000) == 0){
        self.completionHandler(YES, statusBitsString, nil);
        self.failureHandler = nil;
        [self.socket disconnect];
    }
    else{
        NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Printer Status Error", nil) code:CCLGprinterManagerPrinterStatusError];
        self.completionHandler(NO, statusBitsString, errorMessage);
        return;
    }
}



- (void)reloadRealTimeStatusWithSocket:(GCDAsyncSocket *)socket {
    if(!socket){
        NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Printer NetWork Error", nil) code:CCLGprinterManagerNetWorkError];
        self.failureHandler(errorMessage);
        return;
    }
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    
    [self.socket writeData:[CCLGprinterCommand commandPrinterRealTimeStatus] withTimeout:-1 tag:CCLGprinterCommandTypeRealTimeStatus];
    
#pragma clang diagnostic pop
}

- (NSString *)printerStatusFromData:(NSData *)data {
    NSMutableString *result = [NSMutableString string];
    const uint8_t *bytes = [data bytes];
    for (NSInteger i = data.length-1; i >= 0 ; i--) {
        uint8_t byte = bytes[i];
        for (int j = 7; j >= 0; j--) {
            ((byte >> j) & 1) == 0 ? [result appendString:@"0"] : [result appendString:@"1"];
        }
    }
    return result;
}

- (NSString *)getBitsFromData:(NSData *)data {
    NSMutableString *result = [NSMutableString string];
    const uint8_t *bytes = [data bytes];
    for (NSUInteger i = 0; i < data.length; i++) {
        uint8_t byte = bytes[i];
        for (int j = 7; j >= 0; j--) {
            ((byte >> j) & 1) == 0 ? [result appendString:@"0"] : [result appendString:@"1"];
        }
    }
    return result;
}

- (NSError*)errorWithMessage:(NSString*)message code:(CCLGprinterManagerError)errorCode {
    NSDictionary* errorMessage = [NSDictionary dictionaryWithObject:message forKey:NSLocalizedDescriptionKey];
    return [NSError errorWithDomain:CCLGprinterHelperErrorDomain code:errorCode userInfo:errorMessage];
}

#pragma mark - Getter

- (GCDAsyncSocket *)socket {
    if(_socket == nil){
        _socket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    }
    return _socket;
}

#pragma mark - Getter

- (void)setCompletionHandler:(CCLGprinterHelperCompletionBlock)completionHandler {
    [_completionHandler release];
    _completionHandler = [completionHandler copy];
}

- (void)setFailureHandler:(CCLGprinterHelperFailureBlock)failureHandler {
    [_failureHandler release];
    _failureHandler = [failureHandler copy];
}

#pragma mark - 開錢箱  ///H.T.Lin

- (void)openCarshBoxToHost:(NSString *)host
                      port:(uint16_t)port
         completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler
            failureHandler:(CCLGprinterHelperFailureBlock)failureHandler {
     
    NSMutableData *writeData = [[[NSMutableData alloc] init] autorelease];
    //0x1b, 0x70, 0x00, 0x64, 0x64
    [writeData appendData:[CCLGprinterCommand commandOpenCrashBox]];
    
    [self sendDataToHost:host port:port data:writeData completionHandler:completionHandler failureHandler:failureHandler];
}

#pragma mark - 蜂鳴器  ///H.T.Lin

- (void)makeVoiceToHost:(NSString *)host
                   port:(uint16_t)port
      completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler
         failureHandler:(CCLGprinterHelperFailureBlock)failureHandler {
     
    NSMutableData *writeData = [[[NSMutableData alloc] init] autorelease];
   
    [writeData appendData:[CCLGprinterCommand commandVoice]];
    
    [self sendDataToHost:host port:port data:writeData completionHandler:completionHandler failureHandler:failureHandler];
}

#pragma mark - C.C.Chang

+ (BOOL)isStringNotEmpty:(NSString *)string {
    if (((NSNull *)string == [NSNull null]) || (string == nil)) {
        return NO;
    }
    if([string isKindOfClass:[NSString class]]){
        string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if([string length] == 0) { //string is empty or nil
            return NO;
        }
    }
    
    return YES;
}


- (void)scanPrintersWithHandler:(CCLGprinterHelperScanBtPrinterBlock)handler {
    self.scanPrinterHandler = handler;
    
    self.isKeepScan = YES;
    [self _startScan];
}

- (void)_startScan {
    if(self.centerManager.state == CBCentralManagerStatePoweredOn) {
        NSDictionary *option = @{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES };
        [self.centerManager scanForPeripheralsWithServices:nil options:option];
        
        //搜尋指定服務的裝置
        //NSArray *uuidArray= @[ [CBUUID UUIDWithString:CCLBLEDeviceServiceUUID] ];
        //[self.centerManager scanForPeripheralsWithServices:uuidArray options:option];
        
        [self _startTimeoutTimer];
    }
}

- (void)stopScan {
    self.isKeepScan = NO;
    [self _stopScan];
    
    self.scanPrinterHandler = nil;
}

- (void)_stopScan {
    [self _stopTimeoutTimer];
    [self.centerManager stopScan];
    
    [self willChangeValueForKey:@"discoveredPeripherals"];
    [self.discoveredPeripherals removeAllObjects];
    [self didChangeValueForKey:@"discoveredPeripherals"];
}

- (void)_startTimeoutTimer {
    if(timeoutTimer){
        dispatch_source_cancel(timeoutTimer);
        dispatch_release(timeoutTimer);
    }
    
    uint64_t interval = 3 * NSEC_PER_SEC;   //3秒重複
    //dispatch_queue_t queue = dispatch_queue_create("Bluetooth Timeout Queue", 0);
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    timeoutTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    //設定3秒後執行第一次後每3秒重複
    dispatch_source_set_timer(timeoutTimer, dispatch_time(DISPATCH_TIME_NOW, interval), interval, 0);
    dispatch_source_set_event_handler(timeoutTimer, ^() {
        [self _scanTimeoutFunction];
    });
    dispatch_source_set_cancel_handler(timeoutTimer, ^{
        self->timeoutTimer = nil;
    });
    dispatch_resume(timeoutTimer);
}

- (void)_stopTimeoutTimer {
    if(timeoutTimer){
        dispatch_source_cancel(timeoutTimer);
        dispatch_release(timeoutTimer);
    }
    timeoutTimer = nil;
}

- (void)_scanTimeoutFunction {
    [self.discoveredPeripherals enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
        CCCBluetoothGprinter *bluetoothPrinter = obj;
        NSTimeInterval currentTimestamp = [[NSDate date] timeIntervalSince1970];
        if(currentTimestamp - bluetoothPrinter.timestamp > 3.0){
            *stop = YES;
            if(*stop) {
                [self willChangeValueForKey:@"discoveredPeripherals"];
                [self.discoveredPeripherals removeObject:bluetoothPrinter];
                [self didChangeValueForKey:@"discoveredPeripherals"];
                
                if (self.scanPrinterHandler) {
                    self.scanPrinterHandler(bluetoothPrinter, YES);
                }
            }
        }
    }];
}

- (CCCBluetoothGprinter *)connectedCCCBluetoothGprinterWithPeripheral:(CBPeripheral *)peripheral {
    CCCBluetoothGprinter *bluetoothPrinter = nil;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.identifier == %@", peripheral.identifier];
    NSArray *filteredArray = [self.connectedPeripherals filteredArrayUsingPredicate:predicate];
    if (filteredArray.count > 0) {
        bluetoothPrinter = [filteredArray firstObject];
    }
    
    return bluetoothPrinter;
}

#pragma mark - 連線＆斷線

- (void)connectToPrinter:(CCCBluetoothGprinter *)printer
                 timeout:(NSTimeInterval)timeoutInterval
              completion:(void(^)(NSError *))completionHandler {
     
    [self.customErrors removeObjectForKey:printer.identifier];
     
     if (!printer || !printer.peripheral) {
         if (completionHandler) {
             NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Could not connect to device", nil)];
             completionHandler(errorMessage);
         }
         return;
     }
     if ([self.connectedPeripherals containsObject:printer]) {
         if (completionHandler) {
             NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Device connected", nil)];
             completionHandler(errorMessage);
         }
         return;
     }
     
     printer.completionHandler = completionHandler;
     
     [self willChangeValueForKey:@"connectedPeripherals"];
     [self.connectedPeripherals addObject:printer];
     [self didChangeValueForKey:@"connectedPeripherals"];
     
     [self willChangeValueForKey:@"discoveredPeripherals"];
     [self.discoveredPeripherals removeObject:printer];
     [self didChangeValueForKey:@"discoveredPeripherals"];
     
     [self.centerManager connectPeripheral:printer.peripheral
                                   options:@{CBConnectPeripheralOptionNotifyOnConnectionKey:@YES,
                                             CBConnectPeripheralOptionNotifyOnDisconnectionKey:@YES}];
    
     if (timeoutInterval > 0) {
         // Start connection timeout timer
         [self performSelector:@selector(connectionTimeout:) withObject:printer afterDelay:timeoutInterval];
     }
}

- (void)connectionTimeout:(CCCBluetoothGprinter *)printer {
    if (printer.peripheral.state != CBPeripheralStateConnected) {
        [self connectionTimeoutAndDisconnectPeripheral:printer
                                          errorMessage:NSLocalizedString(@"Cannot connect to device", @"")];
    }
}

- (void)disconnectPrinter:(CCCBluetoothGprinter *)printer
               completion:(void(^)(NSError *))completionHandler {
    if (!printer || !printer.peripheral) {
        if (completionHandler) {
            NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Could not disconnect from device", nil)];
            completionHandler(errorMessage);
        }
        
        [self.customErrors removeObjectForKey:printer.identifier];
        return;
    }
    if (![self.connectedPeripherals containsObject:printer]) {
        if (completionHandler) {
            NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Device disconnected", nil)];
            completionHandler(errorMessage);
        }
     
        [self.customErrors removeObjectForKey:printer.identifier];
        return;
    }
 
    printer.completionHandler = completionHandler;
 
    [self.centerManager cancelPeripheralConnection:printer.peripheral];
}

- (void)connectionTimeoutAndDisconnectPeripheral:(CCCBluetoothGprinter *)printer
                                    errorMessage:(NSString *)errorMessage {
 
    self.customErrors[printer.identifier] = [self errorWithMessage:errorMessage];
 
    [self disconnectPrinter:printer
                 completion:printer.completionHandler];
}

#pragma mark -

- (CCCBluetoothGprinter *)bluetoothPrinterWithUUID:(NSUUID *)uuid {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.identifier == %@", uuid];
    NSArray *filteredArray = [self.discoveredPeripherals filteredArrayUsingPredicate:predicate];
    if (filteredArray.count > 0) {
        return [filteredArray firstObject];
    }
    
    filteredArray = [self.connectedPeripherals filteredArrayUsingPredicate:predicate];
    if (filteredArray.count > 0) {
        return [filteredArray firstObject];
    }
    
    CBPeripheral *peripheral = [self.centerManager retrievePeripheralsWithIdentifiers:@[uuid]].firstObject;
    if (!peripheral) {
        return nil;
    }
    
    CCCBluetoothGprinter *printer = [[CCCBluetoothGprinter alloc] init];
    printer.deviceName = peripheral.name;
    printer.identifier = peripheral.identifier;
    printer.peripheral = peripheral;
    printer.timestamp = [[NSDate date] timeIntervalSince1970];
    
    return printer;
}

- (void)setPrintLabelSize:(CGSize)labelSize {
    self.labelSize = labelSize;
}

- (void)sendDataToBluetoothPrinter:(CCCBluetoothGprinter *)printer
                              data:(NSData *)data
                 completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler
                    failureHandler:(CCLGprinterHelperFailureBlock)failureHandler {
    if (!printer || !printer.peripheral) {
        if (failureHandler) {
            NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Invalid printer", nil) code:CCLGprinterManagerBadParamError];
            failureHandler(errorMessage);
        }
        return;
    }
    if (data.length == 0) {
        if (failureHandler) {
            NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Invalid image", nil) code:CCLGprinterManagerBadParamError];
            failureHandler(errorMessage);
        }
        return;
    }
    
    self.writeData = data;
    
    if (printer.peripheral.state == CBPeripheralStateConnected) {
        self.completionHandler = completionHandler;
        self.failureHandler = failureHandler;
        [self beginBluetoothSendProcess:printer];
    }
    else {
        void(^connect)(NSError *) = ^(NSError *error) {
            if (error) {
                if (failureHandler) {
                    failureHandler(error);
                }
                
                self.writeData = nil;
                return;
            }

            self.completionHandler = completionHandler;
            self.failureHandler = failureHandler;
            [self beginBluetoothSendProcess:printer];
            
        };
        
        [self connectToPrinter:printer timeout:5.0 completion:connect];
    }
}

- (void)sendDataToBluetoothPrinter:(CCCBluetoothGprinter *)printer
                             image:(UIImage *)image
                 completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler
                    failureHandler:(CCLGprinterHelperFailureBlock)failureHandler {
    if (!printer || !printer.peripheral) {
        if (failureHandler) {
            NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Invalid printer", nil) code:CCLGprinterManagerBadParamError];
            failureHandler(errorMessage);
        }
        return;
    }
    if (!image) {
        if (failureHandler) {
            NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Invalid image", nil) code:CCLGprinterManagerBadParamError];
            failureHandler(errorMessage);
        }
        return;
    }
    
    NSMutableData *writeData = [[[NSMutableData alloc] init] autorelease];
    [writeData appendData:[CCCGpTsplCommand defaultSettings]];
    [writeData appendData:[CCCGpTsplCommand printAreaSizeWithWidth:self.labelSize.width height:self.labelSize.height]];
    [writeData appendData:[CCCGpTsplCommand gapWithDistance:2 offset:0]];
    [writeData appendData:[CCCGpTsplCommand printDirection:CCCGpTsplCommandDirectionNormal mirror:CCCGpTsplCommandMirrorTypeNormal]];
    [writeData appendData:[CCCGpTsplCommand printImage:image mode:CCCGpTsplCommandBitmapModeOverwrite]];
    [writeData appendData:[CCCGpTsplCommand printWithSets:1 copies:1]];
    
    [self sendDataToBluetoothPrinter:printer
                                data:writeData
                   completionHandler:completionHandler
                      failureHandler:failureHandler];
}

- (void)sendDataToBluetoothPrinter:(CCCBluetoothGprinter *)printer
                            images:(NSArray<UIImage *> *)images
                 completionHandler:(CCLGprinterHelperCompletionBlock)completionHandler
                    failureHandler:(CCLGprinterHelperFailureBlock)failureHandler {
    if (!printer || !printer.peripheral) {
        if (failureHandler) {
            NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Invalid printer", nil) code:CCLGprinterManagerBadParamError];
            failureHandler(errorMessage);
        }
        return;
    }
    if (images.count == 0) {
        if (failureHandler) {
            NSError *errorMessage = [self errorWithMessage:NSLocalizedString(@"Invalid image", nil) code:CCLGprinterManagerBadParamError];
            failureHandler(errorMessage);
        }
        return;
    }
    
    NSMutableData *writeData = [[[NSMutableData alloc] init] autorelease];
    [writeData appendData:[CCCGpTsplCommand defaultSettings]];
    [writeData appendData:[CCCGpTsplCommand printAreaSizeWithWidth:self.labelSize.width height:self.labelSize.height]];
    [writeData appendData:[CCCGpTsplCommand gapWithDistance:2 offset:0]];
    [writeData appendData:[CCCGpTsplCommand printDirection:CCCGpTsplCommandDirectionNormal mirror:CCCGpTsplCommandMirrorTypeNormal]];
    for (UIImage *image in images) {
        [writeData appendData:[CCCGpTsplCommand printImage:image mode:CCCGpTsplCommandBitmapModeOverwrite]];
        [writeData appendData:[CCCGpTsplCommand printWithSets:1 copies:1]];
    }
    
    [self sendDataToBluetoothPrinter:printer
                                data:writeData
                   completionHandler:completionHandler
                      failureHandler:failureHandler];
}

- (void)beginBluetoothSendProcess:(CCCBluetoothGprinter *)printer {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        __block NSError *errorMessage = nil;
        if (![printer enableNotification]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // Disconnect?
                
                if (self.failureHandler) {
                    NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"Cannot enable notification", nil), NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Cannot enable notification", nil)};
                    NSError *error = [NSError errorWithDomain:@"BtSerialOperationError" code:-8766 userInfo:userInfo];
                    self.failureHandler(error);
                }

                self.writeData = nil;
                self.completionHandler = nil;
                self.failureHandler = nil;
            });
            return;
        }
        
        dispatch_semaphore_t signal = dispatch_semaphore_create(0);
        
        [printer getPrinterStatusWithHandler:^(CCCBluetoothGprinterStatus status, NSError *error) {
            if (error) {
                errorMessage = error;
            }
            else if (status != CCCBluetoothGprinterStatusNormal) {
                NSMutableArray<NSString *> *errorStrings = [NSMutableArray array];
                if (status & CCCBluetoothGprinterStatusHeadOpened) {
                    [errorStrings addObject:NSLocalizedString(@"Head opened", nil)];
                }
                if (status & CCCBluetoothGprinterStatusPaperJam) {
                    [errorStrings addObject:NSLocalizedString(@"Paper Jam", nil)];
                }
                if (status & CCCBluetoothGprinterStatusOutOfPaper) {
                    [errorStrings addObject:NSLocalizedString(@"Out of paper", nil)];
                }
                if (status & CCCBluetoothGprinterStatusOutOfRibbon) {
                    [errorStrings addObject:NSLocalizedString(@"Out of ribbon", nil)];
                }
                if (status & CCCBluetoothGprinterStatusPaused) {
                    [errorStrings addObject:NSLocalizedString(@"Paused", nil)];
                }
                if (status & CCCBluetoothGprinterStatusCoverOpened) {
                    [errorStrings addObject:NSLocalizedString(@"Cover opened", nil)];
                }
                
                NSDictionary *userInfo = @{NSLocalizedDescriptionKey: [errorStrings componentsJoinedByString:@", "],
                                           NSLocalizedFailureReasonErrorKey: [errorStrings componentsJoinedByString:@", "]};
                errorMessage = [NSError errorWithDomain:@"BtSerialOperationError" code:-8767 userInfo:userInfo];
            }
            dispatch_semaphore_signal(signal);
        }];
        
        dispatch_semaphore_wait(signal, DISPATCH_TIME_FOREVER);
        
        if (errorMessage) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // Disconnect?
                
                if (self.failureHandler) {
                    self.failureHandler(errorMessage);
                }
                
                self.writeData = nil;
                self.completionHandler = nil;
                self.failureHandler = nil;
            });

            dispatch_release(signal);
            return;
        }
        
        if (!self.writeData) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"No data to write", nil),
                                       NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"No data to write", nil)};
            errorMessage = [NSError errorWithDomain:@"BtSerialOperationError" code:-8767 userInfo:userInfo];
            dispatch_async(dispatch_get_main_queue(), ^{
                // Disconnect?
                
                if (self.failureHandler) {
                    self.failureHandler(errorMessage);
                }
                
                self.writeData = nil;
                self.completionHandler = nil;
                self.failureHandler = nil;
            });

            dispatch_release(signal);
            return;
        }
        
        __block NSInteger progressing = 0;
        __block NSInteger sentNum = 0;
        while (progressing < self.writeData.length) {
            sentNum = [self writeBytesToTarget:printer progressing:progressing completion:^(NSError *error) {
                if (error) {
                    NSLog(@"error: %@", error);
                    // Disconnect?
                }
                else {
                    if (sentNum > 0) {
                        progressing += sentNum;
                    }
                    sentNum = 0;
                    
                    dispatch_semaphore_signal(signal);
                }
            }];
            
            if (sentNum < 0) {
                // Disconnect?
                
                return;
            }
            dispatch_semaphore_wait(signal, DISPATCH_TIME_FOREVER);
            
        }
        
        NSLog(@"complete");
        dispatch_async(dispatch_get_main_queue(), ^{
            // Disconnect?
            
            if (self.completionHandler) {
                self.completionHandler(YES, nil, nil);
            }
            
            self.writeData = nil;
            self.completionHandler = nil;
            self.failureHandler = nil;
        });
        
        dispatch_release(signal);
        
    });
}

- (NSInteger)writeBytesToTarget:(CCCBluetoothGprinter *)printer
                    progressing:(NSInteger)progressing
                     completion:(void(^)(NSError *))completion {
    if (!self.writeData) {
        return -1;
    }
    
    NSInteger length = 75;
    //self.cmdData.length;//20;
    if (progressing + length > self.writeData.length) {
        length = self.writeData.length - progressing;
    }
    
    NSData *data = [self.writeData subdataWithRange:NSMakeRange(progressing, length)];
    
    // Sending out n bytes
//    NSLog(@"Sending out %ld bytes", (long)length);
    [printer writeValue:data forCharacteristic:[CBUUID UUIDWithString:CCCBtGpWriteCharacteristic] service:[CBUUID UUIDWithString:CCCBtGpMainService] timeout:5.0 completion:^(id response, NSString *string, NSError *error) {
        
        if (completion) {
            completion(error);
        }
        
    }];
    
    return length;
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    switch (central.state) {
        case CBManagerStateResetting: {
            NSLog(@"state:CBCentralManagerStateResetting");
            
            [self willChangeValueForKey:@"bluetoothOn"];
            _bluetoothOn = NO;
            [self didChangeValueForKey:@"bluetoothOn"];
            
            [self _stopScan];
            break;
        }
        case CBManagerStateUnsupported: {
            NSLog(@"state:CBCentralManagerStateUnsupported");
            
            [self willChangeValueForKey:@"bluetoothOn"];
            _bluetoothOn = NO;
            [self didChangeValueForKey:@"bluetoothOn"];
            
            [self _stopScan];
            break;
        }
        case CBManagerStateUnauthorized: {
            NSLog(@"state:CBCentralManagerStateUnauthorized");
            
            [self willChangeValueForKey:@"bluetoothOn"];
            _bluetoothOn = NO;
            [self didChangeValueForKey:@"bluetoothOn"];
            
            
            [self _stopScan];
            break;
        }
        case CBManagerStatePoweredOff: {
            NSLog(@"state:CBCentralManagerStatePoweredOff");
            
            [self willChangeValueForKey:@"bluetoothOn"];
            _bluetoothOn = NO;
            [self didChangeValueForKey:@"bluetoothOn"];
            
            
            [self _stopScan];
            break;
        }
        case CBManagerStatePoweredOn: {
            NSLog(@"state:CBCentralManagerStatePoweredOn");
            if(self.isKeepScan){
                [self _startScan];
            }
            
            [self willChangeValueForKey:@"bluetoothOn"];
            _bluetoothOn = YES;
            [self didChangeValueForKey:@"bluetoothOn"];
            
            break;
        }
        default:
            NSLog(@"state:CBCentralManagerStateUnknown");
            
            [self willChangeValueForKey:@"bluetoothOn"];
            _bluetoothOn = NO;
            [self didChangeValueForKey:@"bluetoothOn"];
            
            [self _stopScan];
            break;
    }
}

//- (void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary<NSString *, id> *)dict {
//    NSLog(@"%@",dict);
//}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *, id> *)advertisementData RSSI:(NSNumber *)RSSI {
//    NSLog(@"didDiscover:%@", peripheral);
//    NSLog(@"advertisementData:%@", advertisementData);
//    NSLog(@"RSSI:%@", RSSI);
//    NSLog(@"-----------------------------------------");
//
//    if (peripheral.name) {
//        NSLog(@"掃描到裝置 %@",peripheral.name);
//    }
    
    [self willChangeValueForKey:@"discoveredPeripherals"];
    
    CCCBluetoothGprinter *bluetoothPrinter = nil;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.identifier == %@", peripheral.identifier];
    NSArray *filteredArray = [self.discoveredPeripherals filteredArrayUsingPredicate:predicate];
    if (filteredArray.count > 0) {
        bluetoothPrinter = [filteredArray firstObject];
    }
    
    filteredArray = [self.connectedPeripherals filteredArrayUsingPredicate:predicate];
    if (filteredArray.count > 0) {
        bluetoothPrinter = [filteredArray firstObject];
    }
    
    BOOL isNew = NO;
    if (!bluetoothPrinter) {
        bluetoothPrinter = [[CCCBluetoothGprinter alloc] init];
        [self.discoveredPeripherals addObject:bluetoothPrinter];
        
        isNew = YES;
    }
    
    bluetoothPrinter.deviceName = peripheral.name;
    bluetoothPrinter.identifier = peripheral.identifier;
    bluetoothPrinter.peripheral = peripheral;
    bluetoothPrinter.advertisementData = advertisementData;
    bluetoothPrinter.rssi = RSSI;
    bluetoothPrinter.timestamp = [[NSDate date] timeIntervalSince1970];
    
    [self didChangeValueForKey:@"discoveredPeripherals"];
    //NSLog(@"%@", self.discoveredPeripherals);
    
    if (self.scanPrinterHandler && isNew) {
        self.scanPrinterHandler(bluetoothPrinter, NO);
    }
    
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"didConnectPeripheral:%@", peripheral);
    NSLog(@"-----------------------------------------");
    
    CCCBluetoothGprinter *connectedPrinter = [self connectedCCCBluetoothGprinterWithPeripheral:peripheral];
    if (!connectedPrinter) {
        return;
    }
    
    [connectedPrinter configureGatt];
    
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"didFailToConnectPeripheral:%@", peripheral);
    NSLog(@"error:%@", error);
    NSLog(@"-----------------------------------------");
    
    CCCBluetoothGprinter *connectedPrinter = [self connectedCCCBluetoothGprinterWithPeripheral:peripheral];
    if (!connectedPrinter) {
        return;
    }
    
    if (connectedPrinter.completionHandler) {
        connectedPrinter.completionHandler(error);
        connectedPrinter.completionHandler = nil;
    }
    
    [self willChangeValueForKey:@"connectedPeripherals"];
    [self.connectedPeripherals removeObject:connectedPrinter];
    [self didChangeValueForKey:@"connectedPeripherals"];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"didDisconnectPeripheral:%@", peripheral);
    NSLog(@"error:%@", error);
    NSLog(@"-----------------------------------------");
    
    [central cancelPeripheralConnection:peripheral];
    
    CCCBluetoothGprinter *connectedPrinter = [self connectedCCCBluetoothGprinterWithPeripheral:peripheral];
    if (!connectedPrinter) {
        return;
    }
    
    NSError *customError = self.customErrors[connectedPrinter.identifier];
    
    if (connectedPrinter.completionHandler) {
        if (error) {
            connectedPrinter.completionHandler(error);
        }
        else if (customError) {
            connectedPrinter.completionHandler(customError);
        }
        else {
            connectedPrinter.completionHandler(nil);
        }
        connectedPrinter.completionHandler = nil;
    }
    
    [self.customErrors removeObjectForKey:connectedPrinter.identifier];
    
    [self willChangeValueForKey:@"connectedPeripherals"];
    [self.connectedPeripherals removeObject:connectedPrinter];
    [self didChangeValueForKey:@"connectedPeripherals"];
    
}

#pragma mark - KVO

//方法一
+ (BOOL)automaticallyNotifiesObserversForKey:(NSString *)key {
    if ([key isEqualToString:NSStringFromSelector(@selector(connectedPeripherals))]) {
        return NO;
    }
    
    if ([key isEqualToString:NSStringFromSelector(@selector(discoveredPeripherals))]) {
        return NO;
    }
    
    return [super automaticallyNotifiesObserversForKey:key];
}

#pragma mark - NSError

- (NSError *)errorWithMessage:(NSString *)message {
    NSDictionary *errorMessage = @{NSLocalizedDescriptionKey : message,
                                   NSLocalizedFailureReasonErrorKey : message};
    return [NSError errorWithDomain:CCLGprinterHelperErrorDomain code:-8887 userInfo:errorMessage];
}

@end
