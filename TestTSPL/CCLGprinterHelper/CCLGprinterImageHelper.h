//
//  CCLGprinterImageHelper.h
//  CCLGprinter
//
//  Created by realtouch on 2017/3/4.
//  Copyright © 2017年 realtouch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CCLGprinterImageHelper : NSObject

/** UIView 截圖 */
+ (UIImage *)imageByRenderingView:(UIView *)view width:(CGFloat)width;

/** 無損縮放圖片 */
+ (UIImage *)resizeImage:(UIImage *)image withQuality:(CGInterpolationQuality)quality rate:(CGFloat)rate;

/** 使用 Apple API 產生QRCODE */
+ (UIImage *)qrImageForString:(NSString *)qrString;

@end
