//
//  CCLGprinterImageHelper.m
//  CCLGprinter
//
//  Created by realtouch on 2017/3/4.
//  Copyright © 2017年 realtouch. All rights reserved.
//

#import "CCLGprinterImageHelper.h"

#pragma mark - CCLMainViewController

@interface CCLGprinterImageHelper ()

@end

@implementation CCLGprinterImageHelper

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)dealloc {
    [super dealloc];
}

#pragma mark -

+ (UIImage *)imageByRenderingView:(UIView *)view width:(CGFloat)width {
    
    if(![view isKindOfClass:[UIView class]]){
        return nil;
    }
    
    CGSize size;
    if(width != CGRectGetWidth(view.frame)){
        CGFloat height = CGRectGetHeight(view.frame) * width / CGRectGetWidth(view.frame);
        size = CGSizeMake(width, height);
    }
    else{
        size = view.frame.size;
    }
    CGRect region = { CGPointZero , size };
    UIGraphicsBeginImageContextWithOptions(size, NO, 1.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    //通過kCGInterpolationNone的方式進行拉伸，來獲得更清晰的圖片(效果好像沒比較好的樣子?)
    CGContextSetInterpolationQuality(context, kCGInterpolationNone);
    
    [view drawViewHierarchyInRect:region afterScreenUpdates:YES];
   
    
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

#pragma mark -

+ (UIImage *)resizeImage:(UIImage *)image withQuality:(CGInterpolationQuality)quality rate:(CGFloat)rate {
    UIImage *resized = nil;
    CGFloat width = image.size.width * rate;
    CGFloat height = image.size.height * rate;
    
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    CGContextRef context = UIGraphicsGetCurrentContext();
    //通過kCGInterpolationNone的方式進行拉伸，來獲得更清晰的圖片(效果好像沒比較好的樣子?)
    CGContextSetInterpolationQuality(context, quality);
    [image drawInRect:CGRectMake(0, 0, width, height)];
    resized = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resized;
}

#pragma mark -

+ (UIImage *)qrImageForString:(NSString *)qrString {
    // Need to convert the string to a UTF-8 encoded NSData object
    NSData *stringData = [qrString dataUsingEncoding:NSUTF8StringEncoding];
    
    // Create the filter
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    // Set the message content and error-correction level
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"L" forKey:@"inputCorrectionLevel"];
    
    // Send the image back
    CIImage *outputImage = qrFilter.outputImage;
    UIImage *image = [UIImage imageWithCIImage:outputImage
                                         scale:1.
                                   orientation:UIImageOrientationUp];
    return image;
}


@end
