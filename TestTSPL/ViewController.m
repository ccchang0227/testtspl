//
//  ViewController.m
//  TestTSPL
//
//  Created by realtouch's MBP 2019 on 2020/6/5.
//  Copyright © 2020 RealTouchApp Corp. Ltd. All rights reserved.
//

#import "ViewController.h"
#import "CCCBluetoothManager.h"
#import "CCCBluetoothPeripheralDelegate.h"
#import <PrinterSDK/PTCommandTSPL.h>
#import "BtSerialOperation.h"

#import "CCCGpTsplCommand.h"


#define SERVICE     @"49535343-FE7D-4AE5-8FA9-9FAFD205E455"
#define WRITE       @"49535343-8841-43F4-A8D4-ECBE34729BB3"
#define READ        @"49535343-1E4D-4BD9-BA61-23C647249616"

typedef void(^BtSerialOperationCallback)(id _Nullable response, NSString * _Nullable string, NSError * _Nullable error);

@interface ViewController () <CCCBluetoothPeripheralEvent>

@property (readonly, nonatomic) CCCBluetoothManager *btManager;

@property (strong, nonatomic) CCCBluetoothPeripheral * _Nullable peripheral;
@property (strong, nonatomic) CCCBluetoothPeripheralDelegate *btDelegate;

@property (strong, nonatomic) NSMutableData * _Nullable cmdData;

@property (strong, nonatomic) NSOperationQueue *queue;

@property (nonatomic) NSInteger progressing;
@property (nonatomic) NSInteger sentNum;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.btDelegate = [[CCCBluetoothPeripheralDelegate alloc] init];
    
    self.progressing = 0;
    self.sentNum = 0;
    
}

- (CCCBluetoothManager *)btManager {
    return [CCCBluetoothManager sharedInstance];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.btManager addObserver:self forKeyPath:@"discoveredPeripherals" options:NSKeyValueObservingOptionNew context:nil];
    [self.btManager startScan];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.btManager removeObserver:self forKeyPath:@"discoveredPeripherals" context:nil];
    [self.btManager stopScan];
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    if ([keyPath isEqualToString:@"discoveredPeripherals"]) {
        NSArray<CCCBluetoothPeripheral *> *peripherals = change[NSKeyValueChangeNewKey];
        peripherals = [peripherals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"deviceName==%@", @"Printer_D011"]];
        if (peripherals.count > 0) {
            [self.btManager removeObserver:self forKeyPath:@"discoveredPeripherals" context:nil];
            [self.btManager stopScan];
            
            CCCBluetoothPeripheral *peripheral = peripherals[0];
            self.peripheral = peripheral;
            [self.btManager connectPeripheral:peripheral timeout:10.0 completion:^(NSError *error) {
                if (error) {
                    NSLog(@"connect failed: %@", error);
                    self.peripheral = nil;
                }
                else {
                    [self.btDelegate configureGatt:peripheral.peripheral delegate:self];
                }
            }];
        }
        
    }
    
}

#pragma mark -

- (NSOperationQueue *)queue {
    if (_queue == nil) {
        _queue = [[NSOperationQueue alloc] init];
        _queue.maxConcurrentOperationCount = 1;
        [_queue setSuspended:NO];
    }
    return _queue;
}

- (void)addOperation:(BtSerialOperation *)operation completion:(nullable BtSerialOperationCallback)completion {
    __weak BtSerialOperation *weakOp = operation;
    operation.completionBlock = ^{
        if (!weakOp) {
            return;
        }
        
        __strong BtSerialOperation *strongOp = weakOp;
        if (strongOp.isCancelled) {
            return;
        }
        
        if (strongOp.response && [strongOp.response isKindOfClass:[NSError class]]) {
            // Error Occurred
            if (completion) {
                completion(nil, nil, (NSError *)strongOp.response);
            }
            return;
        }
        
        NSData *data = nil;
        if (strongOp.response && [strongOp.response isKindOfClass:[NSData class]]) {
            data = (NSData *)strongOp.response;
        }
        NSString *string = [CCCBluetoothPeripheralDelegate toUtf8String:data];
        if (completion) {
            completion(data, string, nil);
        }
        
    };
    [self.queue addOperation:operation];
    
}

- (void)readValueForCharacteristic:(CBUUID *)characteristicUUID service:(CBUUID *)serviceUUID completion:(nullable BtSerialOperationCallback)completion {
    CBPeripheral *cbPeripheral = self.peripheral.peripheral;
    if (!cbPeripheral) {
        if (completion) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"No peripheral", nil),
                                       NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"No peripheral", nil)};
            NSError *error = [NSError errorWithDomain:@"BtSerialOperationError" code:-8764 userInfo:userInfo];
            completion(nil, nil, error);
        }
        return;
    }
    CBCharacteristic *characteristic = [self.btDelegate findCharacteristicWithUUID:characteristicUUID inServiceUUID:serviceUUID];
    if (!characteristic) {
        if (completion) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"Characteristic not found", nil),
                                       NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Characteristic not found", nil)};
            NSError *error = [NSError errorWithDomain:@"BtSerialOperationError" code:-8765 userInfo:userInfo];
            completion(nil, nil, error);
        }
        return;
    }
    
    BtSerialOperation *operation = [[BtSerialOperation alloc] initWithType:BtSerialOperationTypeRead
                                                                peripheral:cbPeripheral
                                                            characteristic:characteristic];
    [self addOperation:operation completion:completion];
}

- (void)writeValue:(NSData *)value forCharacteristic:(CBUUID *)characteristicUUID service:(CBUUID *)serviceUUID completion:(nullable BtSerialOperationCallback)completion {
    CBPeripheral *cbPeripheral = self.peripheral.peripheral;
    if (!cbPeripheral) {
        if (completion) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"No peripheral", nil),
                                       NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"No peripheral", nil)};
            NSError *error = [NSError errorWithDomain:@"BtSerialOperationError" code:-8764 userInfo:userInfo];
            completion(nil, nil, error);
        }
        return;
    }
    CBCharacteristic *characteristic = [self.btDelegate findCharacteristicWithUUID:characteristicUUID inServiceUUID:serviceUUID];
    if (!characteristic) {
        if (completion) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"Characteristic not found", nil),
                                       NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Characteristic not found", nil)};
            NSError *error = [NSError errorWithDomain:@"BtSerialOperationError" code:-8765 userInfo:userInfo];
            completion(nil, nil, error);
        }
        return;
    }
    
    CBCharacteristicWriteType writeType = CBCharacteristicWriteWithResponse;
    if ((characteristic.properties & CBCharacteristicWriteWithResponse) == 0) {
        writeType = CBCharacteristicWriteWithoutResponse;
    }
    
    BtSerialOperation *operation = [[BtSerialOperation alloc] initWithType:BtSerialOperationTypeWrite
                                                                peripheral:cbPeripheral
                                                            characteristic:characteristic
                                                                     value:value
                                                                 writeType:writeType];
    [self addOperation:operation completion:completion];
}

- (void)setNotify:(BOOL)enabled forCharacteristic:(CBUUID *)characteristicUUID service:(CBUUID *)serviceUUID completion:(nullable BtSerialOperationCallback)completion {
    CBPeripheral *cbPeripheral = self.peripheral.peripheral;
    if (!cbPeripheral) {
        if (completion) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"No peripheral", nil),
                                       NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"No peripheral", nil)};
            NSError *error = [NSError errorWithDomain:@"BtSerialOperationError" code:-8764 userInfo:userInfo];
            completion(nil, nil, error);
        }
        return;
    }
    CBCharacteristic *characteristic = [self.btDelegate findCharacteristicWithUUID:characteristicUUID inServiceUUID:serviceUUID];
    if (!characteristic) {
        if (completion) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"Characteristic not found", nil),
                                       NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Characteristic not found", nil)};
            NSError *error = [NSError errorWithDomain:@"BtSerialOperationError" code:-8765 userInfo:userInfo];
            completion(nil, nil, error);
        }
        return;
    }
    
    BtSerialOperation *operation = [[BtSerialOperation alloc] initWithType:BtSerialOperationTypeNotify
                                                                peripheral:cbPeripheral
                                                            characteristic:characteristic
                                                                     value:@(enabled)
                                                                 writeType:CBCharacteristicWriteWithResponse];
    [self addOperation:operation completion:completion];
}

#pragma mark - CCCBluetoothPeripheralEvent

- (void)didReadRSSI:(nonnull NSNumber *)RSSI {
//    NSLog(@"%s", __FUNCTION__);
}

- (void)didReadValue:(nullable NSData *)value forCharacteristic:(nonnull CBCharacteristic *)characteristic {
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"Read: %@, for %@", value, characteristic);
    NSLog(@"-------------------------------------------------");
    
    BtSerialOperation *operation = self.queue.operations.firstObject;
    if (operation && [operation isKindOfClass:[BtSerialOperation class]]) {
        if (operation.type == BtSerialOperationTypeRead) {
            [operation completeWithResponse:value];
        }
    }
    
}

- (void)didSearchGattStructure {
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"-------------------------------------------------");
    
    UIImage *image = [UIImage imageNamed:@"testLabel.png"];
    
//    PTCommandTSPL *cmd = [[PTCommandTSPL alloc] init];
//    [cmd setCLS];
//    [cmd setPrintSpeed:PTTSCPrintSpeed4];
//    [cmd setReferenceXPos:0 yPos:0];
//    [cmd setOffsetWithDistance:0];
//    [cmd setPrintDensity:8];
//    [cmd setCLS];
////    [cmd setPrinterStateAutomaticBackWithStatus:YES];
//    [cmd setPrintAreaSizeWithWidth:40 height:30];
//    [cmd setGapWithDistance:2 offset:0];
//    [cmd setPrintDirection:PTTSCPrintDirectionNormal mirror:PTTSCPrintStyleNormal];
//    [cmd addBitmapWithXPos:0 yPos:0 mode:PTTSCBitmapModeOVERWRITE image:image.CGImage bitmapMode:PTBitmapModeBinary compress:PTBitmapCompressModeNone];
////    [cmd printQRcodeWithXPos:10 yPos:10 eccLevel:PTTSCQRcodeEcclevelM cellWidth:PTTSCQRcodeWidth5 mode:PTTSCQRCodeModeAuto rotation:PTTSCStyleRotation0 model:PTTSCQRCodeModelM1 mask:PTTSCQRcodeMaskS7 context:@"http://cwedding.co"];
////    [cmd printBarcodeWithXPos:10 yPos:10 type:PTTSCBarcodeStyle39 height:20 readable:PTTSCBarcodeReadbleStyleHuman rotation:PTTSCStyleRotation0 ratio:PTTSCBarcodeRatio2 context:@"12349129381293"];
//    [cmd printWithSets:1 copies:1];
//    self.cmdData = cmd.cmdData;
    
    NSMutableData *data = [NSMutableData dataWithData:[CCCGpTsplCommand defaultSettings]];
    [data appendData:[CCCGpTsplCommand printAreaSizeWithWidth:40 height:30]];
    [data appendData:[CCCGpTsplCommand gapWithDistance:2 offset:0]];
    [data appendData:[CCCGpTsplCommand printDirection:CCCGpTsplCommandDirectionNormal mirror:CCCGpTsplCommandMirrorTypeNormal]];
    [data appendData:[CCCGpTsplCommand printImage:image mode:CCCGpTsplCommandBitmapModeOverwrite]];
    [data appendData:[CCCGpTsplCommand printWithSets:1 copies:1]];
    self.cmdData = data;
    
    __weak typeof(self) weakSelf = self;
    [self setNotify:YES forCharacteristic:[CBUUID UUIDWithString:READ] service:[CBUUID UUIDWithString:SERVICE] completion:^(id  _Nullable response, NSString * _Nullable string, NSError * _Nullable error) {
        
        if (!weakSelf) {
            return;
        }
        __strong typeof(weakSelf) strongSelf = weakSelf;
        
        NSLog(@"enable notification: %@", READ);
        
        [strongSelf test];
        
    }];
    
}

// TODO: 把藍牙功能加到CCLGprinterHelper

- (void)didUpdateNotificationState:(BOOL)isNotifying forCharacteristic:(nonnull CBCharacteristic *)characteristic {
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"Update Notify: %d, for %@", isNotifying, characteristic);
    NSLog(@"-------------------------------------------------");
    
    BtSerialOperation *operation = self.queue.operations.firstObject;
    if (operation && [operation isKindOfClass:[BtSerialOperation class]]) {
        if (operation.type == BtSerialOperationTypeNotify) {
            [operation completeWithResponse:@(isNotifying)];
        }
    }
    
}

- (void)didWriteValueForCharacteristic:(nonnull CBCharacteristic *)characteristic {
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"write for %@", characteristic);
    NSLog(@"-------------------------------------------------");
    
    BtSerialOperation *operation = self.queue.operations.firstObject;
    if (operation && [operation isKindOfClass:[BtSerialOperation class]]) {
        if (operation.type == BtSerialOperationTypeWrite) {
            [operation complete];
        }
    }
    
}

- (void)gattErrorWithType:(CCCBluetoothEventType)type error:(nonnull NSError *)error {
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"GATT error: %@, type: %ld", error, (long)type);
    NSLog(@"-------------------------------------------------");
    
    BtSerialOperation *operation = self.queue.operations.firstObject;
    if (operation) {
        if (type == CCCBluetoothEventTypeWriteCharacteristic && operation.type == BtSerialOperationTypeWrite) {
            [operation completeWithResponse:error];
        }
        else if (type == CCCBluetoothEventTypeReadCharacteristic && operation.type == BtSerialOperationTypeRead) {
            [operation completeWithResponse:error];
        }
        else if (type == CCCBluetoothEventTypeNotify && operation.type == BtSerialOperationTypeNotify) {
            [operation completeWithResponse:error];
        }
    }
    
}

- (void)isReadytoSendWrite {
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"-------------------------------------------------");
    
    BtSerialOperation *operation = self.queue.operations.firstObject;
    if (operation && [operation isKindOfClass:[BtSerialOperation class]]) {
        if (operation.type == BtSerialOperationTypeWrite) {
            [operation complete];
        }
    }
    
}

#pragma mark -

- (void)test {
//    self.progressing = 0;
//    self.sentNum = 0;
//    [self sendNextImageChunk];
//    return;
    
    
    PTCommandTSPL *cmd = [[PTCommandTSPL alloc] init];
    // This command inquires the model name and number of the printer. This information is returned in ASCII characters.
    cmd.cmdData = [NSMutableData dataWithData:[@"~!T" dataUsingEncoding:NSASCIIStringEncoding]];
    // MODEL:GP-2120
    
    // The command inquires the code page and country setting of the printer.
//    cmd.cmdData = [NSMutableData dataWithData:[@"~!I" dataUsingEncoding:NSASCIIStringEncoding]];
    // CodePage:437
    
    // 该指令用于询问打印机内存大小，回传值以10进制字符表示，以0x0d做为结尾.
//    cmd.cmdData = [NSMutableData dataWithData:[@"~!A" dataUsingEncoding:NSASCIIStringEncoding]];
    // 4452414d 20465245 45204259 5445284b 42293a30 20464c41 53482046 52454520 42595445 284b4229 3a300d0a
    // DRAM FREE BYTE(KB):0 FLASH FREE BYTE(KB):0
    
    __weak typeof(self) weakSelf = self;
    [self writeValue:cmd.cmdData forCharacteristic:[CBUUID UUIDWithString:WRITE] service:[CBUUID UUIDWithString:SERVICE] completion:^(id  _Nullable response, NSString * _Nullable string, NSError * _Nullable error) {

        if (!weakSelf) {
            return;
        }
        __strong typeof(weakSelf) strongSelf = weakSelf;

//        NSLog(@"get printer status");
//        strongSelf.progressing = 0;
//        strongSelf.sentNum = 0;
//        [strongSelf sendNextImageChunk];

    }];
    
}

- (NSInteger)writeBytesToTarget {
    if (!self.cmdData) {
        return -1;
    }
    
    NSInteger length = 75;//self.cmdData.length;//20;
    if (self.progressing + length > self.cmdData.length) {
        length = self.cmdData.length - self.progressing;
    }
    
    NSData *data = [self.cmdData subdataWithRange:NSMakeRange(self.progressing, length)];
    
    // Sending out 20 bytes
    NSLog(@"Sending out %ld bytes", (long)length);
    [self writeValue:data forCharacteristic:[CBUUID UUIDWithString:WRITE] service:[CBUUID UUIDWithString:SERVICE] completion:^(id  _Nullable response, NSString * _Nullable string, NSError * _Nullable error) {
        
        if (error) {
            NSLog(@"error: %@", error);
            [self.btManager disconnectPeripheral:self.peripheral completion:^(NSError *error) {
                self.peripheral = nil;
            }];
            return;
        }
        
        [self increment];
        [self sendNextImageChunk];
        
    }];
    
    return length;
}

- (void)sendNextImageChunk {
    if (!self.cmdData) {
        return;
    }
    
    if (self.progressing >= self.cmdData.length) {
        self.sentNum = 0;
        self.cmdData = nil;
        
        NSLog(@"complete");
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.btManager disconnectPeripheral:self.peripheral completion:^(NSError *error) {
                self.peripheral = nil;
            }];
        });
        
    }
    else {
        self.sentNum = [self writeBytesToTarget];
    }
}

- (void)increment {
    if (self.sentNum > 0) {
        self.progressing += self.sentNum;
    }
    self.sentNum = 0;
}

@end
